cmake_minimum_required (VERSION 3.16)
project(rttt)

set(CMAKE_EXPORT_COMPILE_COMMANDS "on")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# dependencies

#add_subdirectory(third-party)
include(FetchContent)

find_package(CURL REQUIRED)

find_package(pugixml)

if (NOT pugixml_FOUND)
  FetchContent_Declare(pugixml 
    GIT_REPOSITORY https://github.com/zeux/pugixml GIT_TAG v1.11.4)
  FetchContent_MakeAvailable(pugixml)
endif()

find_package(cpr)

if (NOT cpr_FOUND)
  FetchContent_Declare(cpr 
    GIT_REPOSITORY https://github.com/libcpr/cpr.git
    GIT_TAG 871ed52d350214a034f6ef8a3b8f51c5ce1bd400)
  FetchContent_MakeAvailable(cpr)
endif()

find_package(nlohmann_json)

if (NOT nlohmann_json_FOUND)
  FetchContent_Declare(json
    GIT_REPOSITORY https://github.com/ArthurSonzogni/nlohmann_json_cmake_fetchcontent
    GIT_TAG v3.10.4)

  FetchContent_GetProperties(json)
  if(NOT json_POPULATED)
    FetchContent_Populate(json)
    add_subdirectory(${json_SOURCE_DIR} ${json_BINARY_DIR} EXCLUDE_FROM_ALL)
  endif()
endif()

find_package(ftxui)

if (NOT ftxui_FOUND)
  set(FETCHCONTENT_UPDATES_DISCONNECTED TRUE)
  FetchContent_Declare(ftxui
    GIT_REPOSITORY https://github.com/ArthurSonzogni/ftxui
    GIT_TAG b63aa9e
    #GIT_TAG v3.0.0
  )

  FetchContent_GetProperties(ftxui)
  if(NOT ftxui_POPULATED)
    FetchContent_Populate(ftxui)
    add_subdirectory(${ftxui_SOURCE_DIR} ${ftxui_BINARY_DIR} EXCLUDE_FROM_ALL)
  endif()
  FetchContent_MakeAvailable(ftxui)
endif()


find_package(fmt)
if (NOT fmt_FOUND)
FetchContent_Declare(fmt
  GIT_REPOSITORY https://github.com/fmtlib/fmt.git
  GIT_TAG master
)
FetchContent_MakeAvailable(fmt)
endif()

set (CPPZMQ_BUILD_TESTS OFF CACHE INTERNAL "Turn off cppzmq tests")
find_package(cppzmq)
if (NOT cppzmq_FOUND)
  FetchContent_Declare(cppzmq
    GIT_REPOSITORY https://github.com/zeromq/cppzmq
    GIT_TAG v4.8.1 
  )
  FetchContent_MakeAvailable(cppzmq)
endif()

set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
set(THREADS_PREFER_PTHREAD_FLAG TRUE)
find_package(Threads REQUIRED)

# main
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
# As always with cmake we need special repeating of thingies. You would think this is automatically set by CMAKE_CXX_STANDARD, but they love to use the compiler specific flag, which then confuses lsp, because it uses a different compiler. 1-0 for having to manually repeat versus automating things to be sensible. Luckily I don't work in a field based around automating things, otherwise this would be humiliating 
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++20")

# Set a default build type if none was specified
set(default_build_type "Release")
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  set(default_build_type "Debug")
endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
      STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

# Program name and sources
set (TARGET rttt)
set (SOURCES src/main.cpp)

# Setup executable
add_executable(${TARGET} ${SOURCES})

file(GLOB_RECURSE
  FOUND_HEADERS
  src/*.hpp
)

target_precompile_headers(${TARGET}
  PRIVATE
    <chrono>
    <cpr/cpr.h>
    <deque>
    <fmt/format.h>
    <list>
    <map>
    <memory>
    <nlohmann/json.hpp>
    <optional>
    <pugixml.hpp>
    <queue>
    <random>
    <ranges>
    <regex>
    <stack>
    <string>
    <variant>
    <vector>
    <ftxui/component/captured_mouse.hpp>
    <ftxui/component/component.hpp>
    <ftxui/component/screen_interactive.hpp>
    <ftxui/dom/elements.hpp>
    <ftxui/dom/flexbox_config.hpp>
    <zmq.hpp>
)

target_include_directories(${TARGET}
  PRIVATE src/
  PRIVATE src/rttt/
  PRIVATE test/include/
)

target_link_libraries(${TARGET}
  PRIVATE ${CURL_LIBRARIES}
  PRIVATE cpr::cpr 
  PRIVATE nlohmann_json::nlohmann_json
  PRIVATE Threads::Threads
  PRIVATE pugixml::pugixml
  PRIVATE ftxui::screen
  PRIVATE ftxui::dom
  PRIVATE ftxui::component # No
  PRIVATE fmt::fmt
  PRIVATE cppzmq
)

# rttt-send
add_executable(rttt-send src/rttt-send.cpp)
target_link_libraries(rttt-send 
  PRIVATE cppzmq
  PRIVATE fmt::fmt
)

# General options
target_compile_options(${TARGET} PRIVATE -Wall -Wextra -Wpedantic -Werror)

install(TARGETS ${TARGET} rttt-send RUNTIME DESTINATION bin)

# Test files
FILE(GLOB TESTFILES test/catch_*.cpp)
foreach(TESTFILE ${TESTFILES})
  get_filename_component(NAME ${TESTFILE} NAME_WE) 
  if (NOT "catch_login" STREQUAL ${NAME} OR EXISTS "test/login_credentials.hpp")
    if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
      add_executable(${NAME} ${TESTFILE})
    else()
      add_executable(${NAME} EXCLUDE_FROM_ALL ${TESTFILE})
    endif()
    target_precompile_headers(${NAME} REUSE_FROM ${TARGET})
    target_include_directories(${NAME} PRIVATE
      .
      test/include/ 
      src/
    )
    target_link_libraries(${NAME} PRIVATE
      ${CURL_LIBRARIES}
      cpr::cpr
      nlohmann_json::nlohmann_json
      Threads::Threads
      pugixml::pugixml
      PRIVATE ftxui::screen
      PRIVATE ftxui::dom
      PRIVATE ftxui::component # No
      PRIVATE fmt::fmt
    )
  endif()
endforeach()
