#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include <nlohmann/json.hpp>

#include "rttt/prompt.hpp"
#include <set>

using namespace rttt;

SCENARIO("Completer works as expected") {
  prompt::completer cmpl;
  REQUIRE(cmpl.next("a") == "");

  cmpl.matches = {"aaa", "aab", "aba", "abb", "baa"};
  REQUIRE(cmpl.next("a") == "aaa");
  REQUIRE(cmpl.next("a") == "aab");
  REQUIRE(cmpl.next("a") == "aba");
  REQUIRE(cmpl.next("a") == "abb");
  REQUIRE(cmpl.next("a") == "aaa");

  REQUIRE(cmpl.next("aa") == "aaa");
  REQUIRE(cmpl.next("aa") == "aab");
  REQUIRE(cmpl.next("aa") == "aaa");

  REQUIRE(cmpl.next("b") == "baa");
  REQUIRE(cmpl.next("b") == "baa");

  // Should have reset after the previous match
  REQUIRE(cmpl.next("aa") == "aaa");

  REQUIRE(cmpl.next("zz") == "");
}

SCENARIO("Completer works correctly with lower and uppercase") {
  prompt::completer cmpl;
  REQUIRE(cmpl.next("a") == "");

  cmpl.matches = {"aaa", "abb", "Aba", "aab", "baa"};

  REQUIRE(cmpl.next("a") == "aaa");
  REQUIRE(cmpl.next("a") == "aab");
  REQUIRE(cmpl.next("a") == "Aba");
  REQUIRE(cmpl.next("a") == "abb");
  REQUIRE(cmpl.next("a") == "aaa");

  REQUIRE(cmpl.next("aa") == "aaa");
  REQUIRE(cmpl.next("aa") == "aab");
  REQUIRE(cmpl.next("aa") == "aaa");

  REQUIRE(cmpl.next("b") == "baa");
  REQUIRE(cmpl.next("b") == "baa");

  // Should have reset after the previous match
  REQUIRE(cmpl.next("aa") == "aaa");

  REQUIRE(cmpl.next("aB") == "Aba");

  REQUIRE(cmpl.next("zz") == "");
}

SCENARIO("We can convert the state to and from json") {
  prompt::state state;
  state.history = {"a", "b"};
  state.completer.matches = {"a", "b", "c"};
  state.history_id = 2;
  nlohmann::json j = state;
  REQUIRE(j["history"].size() == 2);
  REQUIRE(j["completer"]["matches"].size() == 3);
  REQUIRE(j["history_id"] == 2);

  prompt::state state_from_json = j;

  REQUIRE(state_from_json.history.size() == 2);
  REQUIRE(state_from_json.completer.matches.size() == 3);
  REQUIRE(state_from_json.history_id == 2);
}

SCENARIO("History only saves the last entry of a duplicate") {
  prompt::state state;
  state.history = {"a", "b"};
  state.completer.matches = {"a", "b", "c"};
  state.history_id = 2;
  state = prompt::push(std::move(state), "c");
  REQUIRE(state.history.size() == 3);
  REQUIRE(state.history[state.history_id - 1] == "c");

  state = prompt::push(std::move(state), "a");
  REQUIRE(state.history.size() == 3);
  REQUIRE(state.history[state.history_id - 1] == "a");
}
 
