#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt.hpp"
#include "rttt/config.hpp"

SCENARIO("We can parse a path correctly") {
  // TODO: Move this to the source specific tests
  auto p = rttt::parse_path("/hn/news");
  REQUIRE(p.mode == rttt::list_mode::story);
  REQUIRE(p.name == "/hn/news");

  p = rttt::parse_path("/hn/top");
  REQUIRE(p.mode == rttt::list_mode::story);
  REQUIRE(p.basename == "/hn/top");
  REQUIRE(p.name == "/hn/top");

  p = rttt::parse_path("/hn/news/comments/x1");
  REQUIRE(p.mode == rttt::list_mode::comment);
  REQUIRE(p.basename == "/hn/news");
  REQUIRE(p.name == "/hn/news/comments/x1");
  REQUIRE(p.id == "x1");

  p = rttt::parse_path("/r/news");
  REQUIRE(p.mode == rttt::list_mode::story);
  REQUIRE(p.name == "/r/news");

  p = rttt::parse_path("r/news");
  REQUIRE(p.mode == rttt::list_mode::story);
  REQUIRE(p.name == "/r/news");

  p = rttt::parse_path("/r/news/comments/x1");
  REQUIRE(p.mode == rttt::list_mode::comment);
  REQUIRE(p.name == "/r/news/comments/x1");
  REQUIRE(p.basename == "/r/news");
  REQUIRE(p.id == "x1");

}

SCENARIO("We can extract urls from text") {
  std::vector<std::string> v;
  REQUIRE(rttt::extractURL("") == v);
  v = {"https://tiny1.com"};
  std::string str = "Some text followed by an url https://tiny1.com. And another sentence";
  REQUIRE(rttt::extractURL(str) == v);
  REQUIRE(str == std::string("Some text followed by an url https://tiny1.com. And another sentence"));
  v = {"https://tiny2.com", "http://second.com/bla-old"};
  REQUIRE(rttt::extractURL("Some text followed by an url https://tiny2.com. And another http://second.com/bla-old sentence") == v);


  std::string url = "https://news.ycombinator.com/item?id=29665129";
  v = {url};
  REQUIRE(rttt::extractURL(url) == v);
  v = {"https://en.wikipedia.org/wiki/Lignum_vitae"};
  REQUIRE(rttt::extractURL("https://en.wikipedia.org/wiki/Lignum_vitae),") == v);

  str = "The text https://github.com/MarcoLucidi01/ytcast#ytcast";
  v = {"https://github.com/MarcoLucidi01/ytcast#ytcast"};
  REQUIRE(rttt::extractURL(str) == v);

  str = "[https://tiny3.com](https://tiny3.com)";
  v = {"https://tiny3.com", "https://tiny3.com"};
  REQUIRE(rttt::extractURL(str) == v);
}

// Disabled by default, does not work on ci, where there is no file
// access
SCENARIO("We can find a config file", "[.]") {
  auto config = rttt::config::load();
  REQUIRE(config.contains("reddit"));
}
