#include "cpr/redirect.h"
#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "cpr/cpr.h"
#include "nlohmann/json.hpp"

#include "rttt.hpp"
#include "rttt/config.hpp"
#include "rttt/reddit.hpp"
#include "rttt/socket.hpp"

#include "login_credentials.hpp"

/*
 * This file is to play around with the reddit API and Authentication
 *
 * It is a bit of a mess, but useful to play with the different work flows
 */

struct Credentials {
  std::string client_id;
  std::string secret_token;
  std::string username;
  std::string password;
};

// Helper function to login using test script and actual username/password
inline std::string getAccessToken(const Credentials &credentials) {
  auto payload = cpr::Payload{{"grant_type", "password"},
                              {"username", credentials.username},
                              {"password", credentials.password}};
  auto auth = cpr::Authentication {
    credentials.client_id, credentials.secret_token
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
        ,
        cpr::AuthMode::BASIC
#endif
  };
  auto header = cpr::Header{{"User-Agent", "rttt/0.5.0"}};
  auto uri = cpr::Url{"https://www.reddit.com/api/v1/access_token"};
  auto r = cpr::Post(uri, payload, header, auth);

  if (r.status_code != 200) {
    std::cerr << "Could not obtain an access_token for reddit." << std::endl
              << credentials.username << " " << credentials.password << " "
              << credentials.client_id << " " << credentials.secret_token
              << std::endl
              << r.text << std::endl;
    return "";
  }

  assert(r.status_code == 200);

  nlohmann::json j = nlohmann::json::parse(r.text);

  std::cout << r.text << std::endl;

  return j["access_token"];
}

SCENARIO("We can load reddit", "[.]") {
  // This is using an unsafe way, but it is the most straightforward way to test
  // various things
  using namespace rttt;
  // Move these to a setup function
  RedditLogin credentials;
  reddit::state state;
  auto &rcred = state.credentials;

  rcred.access_token = getAccessToken(
      Credentials{credentials.username, credentials.password,
                  credentials.client_id, credentials.secret_token});
  REQUIRE(!rcred.access_token.empty());

  auto front_path = rttt::parse_path("/r/front");
  reddit::requestURI(front_path);

  // Nothing happened yet
  REQUIRE(reddit::items.size() == 0);

  reddit::update(state);

  while (!reddit::update(state)) {
    REQUIRE(reddit::items.size() == 0);
  }
  REQUIRE(reddit::items.size() == 1);

  auto st = std::get<reddit::Story>(reddit::items.at(front_path.name)[0]);
  auto pwc = rttt::parse_path(front_path.name + "/comments/" + st.id);
  reddit::requestURI(pwc);
  while (!reddit::update(state)) {
  }

  st = std::get<reddit::Story>(reddit::items.at(pwc.name)[0]);
  std::cout << st.title << std::endl;
  std::cout << st.fullname << std::endl;
}

SCENARIO("Load reddit using implicit flow", "[.]") {
  // This only grants access for one hour so not useful
  // implementation here is also not complete

  // https://github.com/reddit-archive/reddit/wiki/OAuth2
  std::string client_id = "8yDBiibHONI95SeMWLZspg";
  std::string redirect_uri = "http://localhost:6501";
  auto header = cpr::Header{{"User-Agent", "rttt/0.5.0"}};

  // Use device_id for state?
  std::string state = rttt::config::random_string(25);

  // Build up url
  /*https://www.reddit.com/api/v1/authorize?client_id=CLIENT_ID&response_type=TYPE&
      state=RANDOM_STRING&redirect_uri=URI&duration=DURATION&scope=SCOPE_STRING*/
  // REQUIRE(false); // double check scopes
  std::string url =
      "https://www.reddit.com/api/v1/authorize?client_id=" + client_id +
      "&response_type=token" + "&state=" + state +
      "&redirect_uri=" + redirect_uri + "&scope=read mysubreddits identity";

  auto socket = rttt::socket::listen(6501);

  rttt::openInBrowser(url);

  auto reply_string = rttt::socket::read_reply_and_close(socket);
  std::cout << reply_string << std::endl;
}

// This is the method we use
SCENARIO("Load reddit using refresh token", "[.]") {
  // Note this is the more complicate flow. The implicit flow above is easier to
  // use This has not been fully implemented, but is the way to support login in
  // https://github.com/reddit-archive/reddit/wiki/OAuth2
  std::string client_id = "8yDBiibHONI95SeMWLZspg";
  std::string redirect_uri = "http://localhost:6501";
  auto header = cpr::Header{{"User-Agent", "rttt/0.5.0"}};

  // Use device_id for state?
  std::string state = rttt::config::random_string(25);

  // Build up url
  /*https://www.reddit.com/api/v1/authorize?client_id=CLIENT_ID&response_type=TYPE&
      state=RANDOM_STRING&redirect_uri=URI&duration=DURATION&scope=SCOPE_STRING*/
  // REQUIRE(false); // double check scopes
  std::string url =
      "https://www.reddit.com/api/v1/authorize?client_id=" + client_id +
      "&response_type=code" + "&state=" + state +
      "&redirect_uri=" + redirect_uri + "&duration=permanent" +
      "&scope=read mysubreddits identity";

  auto socket = rttt::socket::listen(6501);

  rttt::openInBrowser(url);

  auto reply_string = rttt::socket::read_reply_and_close(socket);
  std::cout << reply_string << std::endl;
  auto maybe_code = rttt::reddit::try_to_extract_code(reply_string);

  REQUIRE(maybe_code);
  std::cout << maybe_code.value() << std::endl;

  // Request token as before, but with
  // grant_type=authorization_code&code=CODE&redirect_uri=URI
  auto payload = cpr::Payload{{"grant_type", "authorization_code"},
                              {"code", maybe_code.value()},
                              {"redirect_uri", redirect_uri}};

  // Test getting an access token
  // REQUIRE(false);
  // Test that the access token works

  // Note that for later refresh we need different payload:
  // grant_type=refresh_token&refresh_token=TOKEN
  auto auth = cpr::Authentication {
    client_id, ""
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
        ,
        cpr::AuthMode::BASIC
#endif
  };

  auto uri = cpr::Url{"https://www.reddit.com/api/v1/access_token"};
  auto r = cpr::Post(uri, payload, header, auth);
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;
  nlohmann::json j = nlohmann::json::parse(r.text);

  std::cout << j["access_token"] << std::endl;
  std::cout << j["refresh_token"] << std::endl;
  // Without this we won't get all the subreddits
  auto parameters = cpr::Parameters{{"limit", "100"}};

  r = cpr::Get(cpr::Url{"https://oauth.reddit.com/subreddits/mine/subscriber"},
               header, cpr::Bearer{j["access_token"]}, parameters);
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;

  r = cpr::Get(cpr::Url{"https://oauth.reddit.com/api/v1/me"}, header,
               cpr::Bearer{j["access_token"]});
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;

  // For refresh:?
  payload = cpr::Payload{{"grant_type", "refresh_token"},
                         {"refresh_token", j["refresh_token"]}};
  uri = cpr::Url{"https://www.reddit.com/api/v1/access_token"};
  r = cpr::Post(uri, payload, header, auth);

  // I think there is a chance refresh token changes so we need to update that
  // as well
  std::cout << "refreshed? " << std::endl;
  std::cout << j["refresh_token"] << std::endl;
  j = nlohmann::json::parse(r.text);
  std::cout << j["access_token"] << std::endl;
  std::cout << j["refresh_token"] << std::endl;
}

SCENARIO("Retrieve access token for the application only (not logged in)",
         "[.]") {
  // https://github.com/reddit-archive/reddit/wiki/OAuth2#application-only-oauth
  std::string client_id = "8yDBiibHONI95SeMWLZspg";
  std::string device_id = rttt::config::random_string(25);
  auto payload = cpr::Payload{
      {"grant_type", "https://oauth.reddit.com/grants/installed_client"},
      {"device_id", device_id}};

  auto header = cpr::Header{{"User-Agent", "rttt/0.5.0"}};
  // auto auth =
  //    cpr::Authentication{credentials.client_id, credentials.secret_token,
  //    cpr::AuthMode::BASIC};
  auto auth = cpr::Authentication {
    client_id, ""
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
        ,
        cpr::AuthMode::BASIC
#endif
  };

  auto uri = cpr::Url{"https://www.reddit.com/api/v1/access_token"};
  auto r = cpr::Post(uri, payload, header, auth);
  std::cout << r.status_code << std::endl;
  std::cout << nlohmann::json::parse(r.text).dump(2) << std::endl;

  /*if (r.status_code != 200) {
    std::cerr << "Could not obtain an access_token for reddit." << std::endl
              << credentials.username << " " << credentials.password << " "
              << credentials.client_id << " " << credentials.secret_token
              << std::endl
              << r.text << std::endl;
    return "";
  }*/

  REQUIRE(r.status_code == 200);

  nlohmann::json j = nlohmann::json::parse(r.text);

  std::cout << j["access_token"] << std::endl;
  /*r = cpr::Get(cpr::Url{"https://oauth.reddit.com/hot"}, header,
               cpr::Bearer{j["access_token"]});
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;*/

  r = cpr::Get(cpr::Url{"https://oauth.reddit.com/subreddits/mine/subscriber"},
               header, cpr::Bearer{j["access_token"]});
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;

  r = cpr::Get(cpr::Url{"https://oauth.reddit.com/api/v1/me"}, header,
               cpr::Bearer{j["access_token"]});
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;

  r = cpr::Get(cpr::Url{"https://oauth.reddit.com/r/fasting/comments/td0exe"},
               header, cpr::Bearer{j["access_token"]});
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;

  /*
  auto parameters = cpr::Parameters{{"limit", "100"}};

  auto request_function = [&parameters](std::string url) {
    return cpr::Get(cpr::Url{url}, reddit::header,
                       cpr::Bearer{reddit::token}, parameters);
  };
  request_function("https://oauth.reddit.com/hot");
  */
}

SCENARIO("Load reddit using config file", "[.]") {
  using namespace rttt;
  reddit::state state_reddit;
  auto config = rttt::config::load();
  state_reddit =
      rttt::reddit::retrieve_access_token(std::move(state_reddit), config);
  while (state_reddit.credentials.access_token.empty())
    reddit::update(state_reddit);

  auto r =
      cpr::Get(cpr::Url{"https://oauth.reddit.com/r/fasting/comments/td0exe"},
               state_reddit.credentials.header,
               cpr::Bearer{state_reddit.credentials.access_token});

  // std::cout << nlohmann::json::parse(r.text).dump(2) << std::endl;

  /*auto parameters = cpr::Parameters{{"dir", "0"}, {"id", "t1_i0h1fsv"}};
  r = cpr::Post(cpr::Url{"https://oauth.reddit.com/api/vote"},
               state_reddit.credentials.header,
               cpr::Bearer{state_reddit.credentials.access_token},
                parameters);
  std::cout << r.status_code << std::endl;
  std::cout << r.text << std::endl;*/
}

/**
 * twitter
 *
 * Get bearer token:
 * https://developer.twitter.com/en/docs/authentication/oauth-2-0/bearer-tokens
 * User authentication:
 * https://developer.twitter.com/en/docs/authentication/oauth-2-0/user-access-token
 *
 */
SCENARIO("Get bearer token", "[.]") {
  TwitterCredentials crs;

  auto payload = cpr::Payload{{"grant_type", "client_credentials"}};
  auto auth = cpr::Authentication {
    crs.api_key, crs.api_secret_key
#if (CPR_VERSION_MAJOR > 1 || CPR_VERSION_MINOR > 8)
        ,
        cpr::AuthMode::BASIC
#endif
  };

  auto r = cpr::Post(cpr::Url{"https://api.twitter.com/oauth2/token"}, payload,
                     auth);
  std::cout << r.text << std::endl;
}

SCENARIO("Load tweets from a user", "[.]") {
  TwitterCredentials crs;

  // Get tweets from a user
  auto r = cpr::Get(
      cpr::Url{"https://api.twitter.com/2/users/by/username/awesomekling"},
      cpr::Bearer{crs.bearer});
  auto j = nlohmann::json::parse(r.text);
  std::cout << r.text << std::endl;
  std::cout << j["data"]["id"] << std::endl;

  auto parameters = cpr::Parameters{
      {"exclude", "replies,retweets"},
      {"tweet.fields",
       "conversation_id,referenced_tweets,reply_settings,public_metrics"},
      {"max_results", "100"}};
  r = cpr::Get(cpr::Url{"https://api.twitter.com/2/users/" +
                        j["data"]["id"].get<std::string>() + "/tweets"},
               cpr::Bearer{crs.bearer}, parameters);
  std::cout << r.text << std::endl;

  // Get replies to a specific tweet
  std::cout << std::endl;
  parameters = cpr::Parameters{
      {"tweet.fields",
       "conversation_id,referenced_tweets,reply_settings,public_metrics"}};
  r = cpr::Get(cpr::Url{"https://api.twitter.com/2/tweets/1512794180568985601"},
               cpr::Bearer{crs.bearer}, parameters);
  std::cout << r.text << std::endl;

  std::cout << std::endl;
  std::cout << "Using search " << std::endl;
  parameters = cpr::Parameters{
      {"query", "conversation_id:1515239984185450497"},
      {"tweet.fields", "author_id,in_reply_to_user_id,referenced_tweets,reply_"
                       "settings,public_metrics"}};
  r = cpr::Get(cpr::Url{"https://api.twitter.com/2/tweets/search/recent"},
               cpr::Bearer{crs.bearer}, parameters);
  std::cout << r.text << std::endl;
}
