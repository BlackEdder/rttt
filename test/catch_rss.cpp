#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "time.h"

#include <map>
#include <string>
#include <vector>

#include "rttt/future.hpp"
#include "rttt/request.hpp"
#include "rttt/rss.hpp"

using namespace rttt;

SCENARIO("Parse path works as expected") {
  auto p = rss::parse_path("/rss");
  REQUIRE(p.mode == rttt::list_mode::story);
  REQUIRE(p.name == "/rss");
  REQUIRE(p.basename == "/rss");

  p = rss::parse_path("/rss/x1");
  REQUIRE(p.mode == rttt::list_mode::feed);
  REQUIRE(p.name == "/rss/x1");
  REQUIRE(p.basename == "/rss");
}

// rename to parse_opml?
std::map<std::string, std::string> parse_opml(const std::string &filename) {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(filename.c_str());
  assert(result);

  return rttt::rss::parse_opml(doc.child("opml").child("body"));
}

SCENARIO("story_set orders in reverse chronological order") {
  rss::Story s1;
  s1.time = 2;
  s1.id = 2;
  rss::Story s2;
  s2.time = 1;
  s2.id = 1;
  rss::story_set set;
  set.insert(s2);
  set.insert(s1);
  REQUIRE(set.begin()->time == 2);
  REQUIRE(set.size() == 2);
  set.insert(s1);
  REQUIRE(set.size() == 2);

  rss::Story s3;
  s3.time = 2;
  s3.id = 2;
  set.insert(s3);
  REQUIRE(set.size() == 2);

  rss::Story s4;
  s4.time = 2;
  s4.id = 1;
  set.insert(s4);
  REQUIRE(set.size() == 3);
}

SCENARIO("We can parse subscriptions.xml correctly") {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file("test/subscriptions.opml");
  REQUIRE(result);

  auto urls = rttt::rss::parse_opml(doc.child("opml").child("body"));
  REQUIRE(urls.size() == 9);
  REQUIRE(parse_opml("test/subscriptions.opml").size() == 9);
  REQUIRE(!urls.begin()->first.empty());
  REQUIRE(!urls.begin()->second.empty());
}

SCENARIO("We can parse time string") {
  auto time_tm = rttt::parse_time("2022-01-07T12:39:42-08:00");
  REQUIRE(time_tm.tm_year == 2022 - 1900);
  REQUIRE(time_tm.tm_mon == 0);
  REQUIRE(time_tm.tm_mday == 7);
  REQUIRE(time_tm.tm_hour == 12);
  REQUIRE(time_tm.tm_min == 39);
  REQUIRE(time_tm.tm_sec == 0);

  time_tm = rttt::parse_time("2021-12-12T09:00:00+01:00");
  REQUIRE(time_tm.tm_year == 2021 - 1900);
  REQUIRE(time_tm.tm_mon == 11);
  REQUIRE(time_tm.tm_mday == 12);
  REQUIRE(time_tm.tm_hour == 9);
  REQUIRE(time_tm.tm_min == 0);
  REQUIRE(time_tm.tm_sec == 0);
  uint64_t time = std::mktime(&time_tm);

  /*char str[30] ;
  strftime(str, 30, "%Y-%m-%dT%H:%M", &time_tm);
  std::cout << str << std::endl;
  std::cout << std::to_string(mktime(&time_tm)) << std::endl;*/

  time_tm = rttt::rss::parse_pubdate("Wed, 24 Nov 2021 13:46:56 +0000");
  REQUIRE(time_tm.tm_year == 2021 - 1900);
  REQUIRE(time_tm.tm_mon == 10);
  REQUIRE(time_tm.tm_mday == 24);
  REQUIRE(time_tm.tm_hour == 13);
  REQUIRE(time_tm.tm_min == 46);
  REQUIRE(time_tm.tm_sec == 0);

  time_tm = rttt::rss::parse_pubdate("22 Aug 2022");
  REQUIRE(time_tm.tm_year == 2022 - 1900);
  REQUIRE(time_tm.tm_mon == 7);
  REQUIRE(time_tm.tm_mday == 22);
  REQUIRE(time_tm.tm_hour == 0);
  REQUIRE(time_tm.tm_min == 0);
  REQUIRE(time_tm.tm_sec == 0);
}

SCENARIO("We can parse atom correctly") {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file("test/atom.xml");
  REQUIRE(result);
  // Parsing by hand works
  auto feed = doc.child("feed");
  auto by = feed.child("author").child("name").text().get();
  auto blog = feed.child("title").text().get();
  // Parse all the entries, get url title, by, time and text
  std::vector<rss::Story> stories;
  for (pugi::xml_node entry = feed.child("entry"); entry;
       entry = entry.next_sibling("entry")) {
    rss::Story story;
    story.title = entry.child("title").text().get();
    story.text = entry.child("content").text().get();
    story.url = entry.child("link").text().get();
    story.by = by;
    story.domain = blog;
    auto time_tm = rttt::parse_time(entry.child("published").text().get());
    story.time = std::mktime(&time_tm);
    stories.push_back(story);
    /*std::cout << entry.name() << std::endl;
    std::cout << story.title << std::endl;
    std::cout << story.by << std::endl;
    std::cout << story.blog << std::endl;*/
  }
  REQUIRE(stories.size() == 15);

  auto story_set = rttt::rss::parse_atom(doc, "key");
  REQUIRE(story_set.size() == 15);
  REQUIRE(
      story_set.begin()->url ==
      "http://tenderlovemaking.com/2022/01/07/homebrew-rosetta-and-ruby.html");
  // Second feed
  result = doc.load_file("test/dave.xml");

  auto parsed = rss::parse_atom(doc, "dummy");
  auto second = (*(++parsed.begin()));
  REQUIRE(second.url == "https://airlied.blogspot.com/2021/11/"
                        "video-decode-crossing-streams.html");
}

SCENARIO("We can parse rss correctly") {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file("test/rss.xml");
  REQUIRE(result);

  // Parsing by hand works
  auto feed = doc.child("rss").child("channel");
  auto blog = feed.child("title").text().get();
  // Parse all the entries, get url title, by, time and text
  std::vector<rss::Story> stories;
  for (pugi::xml_node entry = feed.child("item"); entry;
       entry = entry.next_sibling("item")) {
    rss::Story story;
    story.title = entry.child("title").text().get();
    story.text = entry.child("description").text().get();
    story.url = entry.child("link").text().get();
    story.by = entry.child("dc:creator").text().get();
    story.domain = blog;
    auto time_tm =
        rttt::rss::parse_pubdate(entry.child("pubDate").text().get());
    story.time = std::mktime(&time_tm);
    stories.push_back(story);
    /*std::cout << entry.name() << std::endl;
    std::cout << story.title << std::endl;
    std::cout << story.by << std::endl;
    std::cout << story.domain << std::endl;*/
  }
  REQUIRE(stories.size() == 10);

  auto story_set = rttt::rss::parse_rss(doc, "key");
  REQUIRE(story_set.size() == 10);
  REQUIRE(story_set.begin()->url ==
          "https://joepusey.com/what-resources-for-uk-medical-school/");

  // lxer has a different formate
  result = doc.load_file("test/lxer.rss");
  REQUIRE(result);
  auto v = rttt::rss::parse_rss(doc, "key");
  REQUIRE(v.size() > 0);
  REQUIRE(v.begin()->time != 0);
  // FIXME: Does not work on CI, but does work on local machine. Not clear why
  if (!(v.begin()->time >= 1642944120 - 48 * 3600 &&
        v.begin()->time <= 1642944120 + 48 * 3600)) {
    std::cout << "Unexpected time: " << v.begin()->time << std::endl;
    REQUIRE(false);
  }
}

SCENARIO("We can detect rss versus atom") {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file("test/rss.xml");
  REQUIRE(result);
  REQUIRE(rttt::rss::is_rss(doc));

  result = doc.load_file("test/atom.xml");
  REQUIRE(result);
  REQUIRE(!rttt::rss::is_rss(doc));

  result = doc.load_file("test/lxer.rss");
  REQUIRE(result);
  REQUIRE(rttt::rss::is_rss(doc));
}

SCENARIO(
    "We can download and parse the subscriptions in test/subscriptions.opml",
    "[.]") {
  rttt::request::State<std::string> state;

  auto feeds = parse_opml("test/subscriptions.opml");

  for (auto &feed : feeds)
    state = rttt::request::push(std::move(state), feed.second, feed.first);

  while (!state.requestQueue.empty() || !state.requestMap.empty()) {
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    state = rttt::request::update(std::move(state));
  }

  auto maybe_feed = rttt::request::try_pop_and_retrieve(state);
  std::vector<rss::Story> stories;
  while (maybe_feed) {
    std::cout << maybe_feed.value().first << std::endl;
    auto urls = rttt::rss::parse_feed(maybe_feed.value().second, "key");
    std::cout << urls.size() << std::endl;
    stories.insert(stories.end(), urls.begin(), urls.end());
    maybe_feed = rttt::request::try_pop_and_retrieve(state);
  }
  REQUIRE(!stories.empty());

  // TODO: disable this test by default
  REQUIRE(false);
}

SCENARIO("Add feed to opml file", "[]") {
  using namespace future;
  cpr::Url url{"https://www.andrewheiss.com/atom.xml"};
  future_wait_list<cpr::Response> wait_list;

  std::string domain;
  std::string orig_opml_file = "test/subscriptions.opml";
  std::string test_opml_file = "test/test_subscriptions.opml";
  std::filesystem::copy_file(orig_opml_file, test_opml_file);
  rss::state state;

  wait_list.push_back(rss_feed_add(state, url, test_opml_file));
  // Download it
  while (!wait_list.empty()) {
    future::update(wait_list);
  }
  // Check that it has been added
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(test_opml_file.c_str());
  REQUIRE(strcmp("Andrew Heiss", doc.child("opml")
                                     .child("body")
                                     .last_child()
                                     .attribute("title")
                                     .value()) == 0);
  std::filesystem::remove(test_opml_file);
  REQUIRE(state.items.size() == 1);
  REQUIRE(state.item_cache.size() > 0);
  REQUIRE(state.feed_urls.size() == 1);
}

// Add feed: for tests we can use https://durstongear.com/feed and
// linus' (Serenity os) and Andrew (see notes about brms)
// https://this-week-in-neovim.org/api/rss
// "https://www.andrewheiss.com/atom.xml"
