#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt.hpp"

SCENARIO("Safe to string works correctly") {
  REQUIRE(rttt::safe_to_string(3126060) == "3126060");
  REQUIRE(rttt::safe_to_string("3126060") == "3126060");
} 
