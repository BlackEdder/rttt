#define CATCH_CONFIG_MAIN

#include <ranges>

#include "rttt/functional.hpp"

#include "catch2/catch.hpp"

SCENARIO("apply functions using with works") {
  struct A {
    double x = 4;
  };
  auto g2 = [](const auto a) { return a.x + 4.0; };
  A a;

  REQUIRE((a | with(g2)) == 8.0);
  REQUIRE((a | with([](auto a) { return a.x + 4.0; })) == 8.0);
  {
    a = a | with([](auto b) {
          b.x += 4.0;
          return b;
        });
  }
  REQUIRE(a.x == 8.0);

  a | with([](auto &b) { b.x += 4.0; });
  REQUIRE(a.x == 12.0);

  auto v = a | with([](auto b) { return b.x; }) |
           with([](auto b) { return b + 4.0; });

  REQUIRE(v == 16.0);

  a = a | with([](auto &value) {
        value.x += 4;
        return value;
      }) |
      with([](auto &value) {
        value.x += 4;
        return value;
      });

  REQUIRE(a.x == 20.0);
}

SCENARIO("We can apply functions to optionals") {
  std::optional<int> a = std::nullopt;
  REQUIRE(!(a | try_with([](auto value) { return value * 2; })).has_value());
  a = 4;
  REQUIRE((a | try_with([](auto value) { return value * 2; })).has_value());
  REQUIRE((a | try_with([](auto value) { return value * 2; })).value() == 8);

  auto x = a | try_with([](auto value) { return value * 2; }) |
           try_with([](auto value) { return value * 2; });
  REQUIRE(x.value() == 16);

  // Works if the function already takes an optional
  auto f = [](std::optional<int> maybe_value) -> std::optional<int> {
    if (maybe_value.has_value())
      return 2 * maybe_value.value();
    else
      return std::nullopt;
  };
  a = std::nullopt;
  REQUIRE(!(a | try_with(f)).has_value());
  a = 4;
  REQUIRE((a | try_with(f)).has_value());
  REQUIRE((a | try_with(f)).value() == 8);

  x = a | try_with(f) | try_with(f);
  REQUIRE(x.value() == 16);
}

SCENARIO("We can apply functions to variants") {
  std::variant<int, std::string> a = "a";

  auto f = [](auto a) {
    if constexpr (std::is_same<std::string, decltype(a)>::value) {
      return 1;
    } else {
      return 2;
    }
  };

  REQUIRE((a | visit_with(f)) == 1);

  a = 4;
  REQUIRE((a | visit_with(f)) == 2);
}

// Testing out functional design
auto plus_one() {
  return with([](auto a) { return a + 1; });
}

SCENARIO("Functional API works") {
  int a = 2;
  auto x = a | plus_one();
  REQUIRE(x == 3);
  x = a | plus_one() | plus_one();
  REQUIRE(x == 4);
}

SCENARIO("Collect view into known type") {
  std::vector<int> v = {1, 2, 3};
  std::vector<int> v1 = v | std::views::take(2) | collect<std::vector<int>>();
  REQUIRE(v1.size() == 2);
}

/*
template <typename T, typename L>
auto with(std::optional<T> const &opt, L const &withFun)
    -> add_optionality<decltype(withFun(*opt))> {
  if (opt)
    return withFun(*opt);
  else
    return std::nullopt;
}

SCENARIO("With works on optional") {
  std::optional<int> a = std::nullopt;
  REQUIRE(!with(a, [](auto value) { return value * 2; }).has_value());

  a = 2;
  REQUIRE(with(a, [](auto value) { return value * 2; }).has_value());
  REQUIRE(with(a, [](auto value) { return value * 2; }).value() == 4);
}*/
/*
template <typename F, typename G> auto operator|(F &f, G &g)
  requires(std::is_invocable<F>::value &&
    std::is_invocable<G, typename std::invoke_result<F>::type>::value
  )
{ return g(f()); }

template <typename T, typename G> auto operator|(T &t, G &g)
  requires(!std::is_invocable<T>::value &&
    std::is_invocable<G, T>::value
  )
{ return g(t); }

SCENARIO("Ranges keep working") {
  std::vector<double> v{1, 2, 3};
  auto vw = v | std::views::transform([&](auto value) { return sqrt(value); });
  REQUIRE(vw.front() == 1);
}

SCENARIO("Currying works") {
  auto f = []() { return 4.0; };
  auto g = [](auto a) { return a + 4.0; };
  REQUIRE((f | g) == 8.0);

  auto f1 = []() { return 4.0; };
  auto g1 = [](const auto a) { return a + 4.0; };
  REQUIRE((f1 | g1) == 8.0);

  struct A {
    double x = 4;
  };
  auto g2 = [](const auto a) { return a.x + 4.0; };
  A a;

  REQUIRE((a | g2) == 8.0);
}
*/
/*
SCENARIO("Currying works") {
  auto f = []() { return "a"; };
  auto g = [](auto a) { return a + 4.0; };
  REQUIRE((f | g) == 8.0);
}
*/
