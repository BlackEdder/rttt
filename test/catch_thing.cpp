#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt/config.hpp"
#include "rttt/thing.hpp"

using namespace rttt;

SCENARIO("We can load and update things/sources") {
  auto config = config::defaultConfig();
  thing::state things;
  thing::setup(things, config);
  for (size_t i = 0; i < 100; ++i) {
    thing::update(things);
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
}
