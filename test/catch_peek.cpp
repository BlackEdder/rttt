#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt.hpp"
#include "rttt/thing.hpp"

namespace rttt {
namespace peek {

struct peek_state {
  std::vector<std::string> paths = {"/r/front", "/rss/Mike Blumenkrantz",
                                    "/hn/top", "/r/funny", "/tw/awesomekling"};
  thing::state *thing_state;
};

using state = std::shared_ptr<peek_state>;

bool is_valid_path(std::string_view path) {
  return path == "/peek";
}

bool is_valid_path(const rttt::Path &path) {
  return path.basename == "/peek";
}

std::optional<rttt::Path> parse_path(std::string_view path) {
  if (peek::is_valid_path(path)) {
    return rttt::parse_path(path);
  }
  return std::nullopt;
}

auto setup(state state, nlohmann::json config, thing::state * tstate) {
  if (!state)
    state = std::make_shared<peek_state>();
  state->thing_state = tstate;
  if (config.contains("peek") && config["peek"].contains("paths")) {
    state->paths = config["peek"]["paths"].get<std::vector<std::string>>();
  }
  return state;
}
} // namespace peek
} // namespace rttt

using namespace rttt;

// TODO: should we add a thing::is_valid_path(state, path), which uses the state to
// infer which type to test for?
//
SCENARIO("We can parse peek path") {
  REQUIRE(peek::is_valid_path("/peek"));
  REQUIRE(!peek::is_valid_path("/r/peek"));

  auto pth = peek::parse_path("/peek");
  REQUIRE(pth.has_value());
  REQUIRE(pth.value().parts[0] == "peek");
  REQUIRE(peek::is_valid_path(pth.value()));

  pth = peek::parse_path("/r/peek");
  REQUIRE(!pth.has_value());
}

SCENARIO("We can use peek state") {
  thing::state thing_state;
  peek::state state;
  nlohmann::json config;
  peek::setup(state, config, &thing_state);
}
