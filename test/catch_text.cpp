#define CATCH_CONFIG_MAIN

#include <algorithm>

#include "catch2/catch.hpp"

#include "rttt/text.hpp"

SCENARIO("We can split a string correctly") {
  // Test split
  auto v = rttt::text::split("/r/front", "/");
  REQUIRE(v.size() == 2);
  REQUIRE(v[0] == "r");
  REQUIRE(v[1] == "front");
}

SCENARIO("Line wrapping works") {
  using namespace rttt::text;
  std::string str =
      "Lorem Ipsum is simply dummy text of the printing and typesetting "
      "industry. Lorem Ipsum has been the industry's standard dummy text "
      "ever since the 1500s, when an unknown printer took a galley of type "
      "and scrambled it to make a type specimen book. 😁";

  auto wrapped = wrap(str, 50);
  REQUIRE(wrapped.size() == 6);
  for (auto &s : wrapped) {
    REQUIRE(s.size() <= 50);
    //std::cout << s << std::endl;
  }

  // Deal with new lines
  str =
      "Lorem Ipsum is simply dummy text of the\nprinting and typesetting "
      "industry. Lorem Ipsum has been the industry's standard dummy text "
      "ever since the 1500s, when an unknown printer took a galley of type "
      "and scrambled it to make a type specimen book. 😁";

  wrapped = wrap(str, 50);
  REQUIRE(wrapped.size() == 6);
  for (auto &s : wrapped) {
    REQUIRE(s.size() <= 50);
    //std::cout << s << std::endl;
  }

  str =
      "Lorem Ipsum is simply dummy text of the printingandtypesetting "
      "industry. Lorem Ipsum has been the industry's standard dummy text "
      "ever since the 1500s, when an unknown printer took a galley of type "
      "and scrambled it to make a type specimen book. 😁";

  wrapped = wrap(str, 50);
  REQUIRE(wrapped.size() == 6);
  for (auto &s : wrapped) {
    REQUIRE(s.size() <= 50);
    //std::cout << s << std::endl;
  }

  wrapped = wrap(str, 40);
  REQUIRE(wrapped.size() == 7);
  for (auto &s : wrapped) {
    REQUIRE(s.size() <= 40);
    //std::cout << s << std::endl;
  }
}
