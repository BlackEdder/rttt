#pragma once

#include <string>
#include <vector>

namespace rttt {
namespace fake_thing {

std::string sample_text =
    "Lorem Ipsum is simply dummy text of the printing and typesetting "
    "industry. Lorem Ipsum has been the industry's standard dummy text "
    "ever since the 1500s, when an unknown printer took a galley of type "
    "and scrambled it to make a type specimen book. 😁";

struct item {
  std::string text;
  std::string id;
};

auto try_path_view() {
  std::vector<std::pair<size_t, item>> items;
  for (auto i = 0; i < 150; ++i) {
    item item;
    item.text = sample_text;
    item.id = std::to_string(i);
    items.push_back({0, item});
  }
  return items;
}
} // namespace fake_thing
} // namespace rttt
