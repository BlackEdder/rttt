#pragma once

#include <algorithm>

#include "rttt.hpp"
#include "rttt/view.hpp"

namespace rttt {
namespace source {
struct state {};

struct item {};

auto setup(state &state, nlohmann::json config) {}

auto update(state &state) {}

bool is_valid_path(const std::string &name) { return false; }

bool is_valid_path(const rttt::Path &path) { return false; }

Path parse_path(const std::string &name) { return rttt::parse_path(name); }

auto retrieve_path_view(state &state, const rttt::Path &path) {
}

auto try_path_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  // Using a vector here, but ideally this returns an optional (lazy) view, e.g.
  // see twitter.hpp
  std::optional<std::variant<std::vector<std::pair<size_t, item>>>> opt;
  return opt;
}
} // namespace source
} // namespace rttt
