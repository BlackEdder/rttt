#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include <random>
#include <ranges>

#include "rttt/storage.hpp"

using namespace rttt;

SCENARIO("We can change a value in place using at()") {
  active_storage<int, std::string> storage;
  storage.insert({10, "a"});
  REQUIRE(!storage.update());
  REQUIRE(storage.size() == 1);
  REQUIRE(storage.at(10) == "a");
  storage.at(10) = "b";
  REQUIRE(storage.at(10) == "b");
}

SCENARIO("We can change a value in place using on_timeout") {
  active_storage<int, std::string> storage;
  storage.insert({10, "a"});
  REQUIRE(storage.at(10) == "a");
  storage.on_timeout = [](auto &key, auto &value) { value = "b"; };
  REQUIRE(storage.update());
  REQUIRE(storage.size() == 1);
  REQUIRE(storage.at(10) == "b");
}

SCENARIO("Nothing happens if on_timeout is not defined") {
  active_storage<int, std::string> storage;
  storage.insert({10, "a"});
  REQUIRE(storage.at(10) == "a");
  REQUIRE(!storage.update());
  REQUIRE(storage.size() == 1);
  REQUIRE(storage.at(10) == "a");
}

SCENARIO("Timing works") {
  active_storage<int, std::string> storage;
  size_t cnt = 0;
  storage.on_timeout = [&cnt](auto key, auto value) { ++cnt; };
  storage.timeout = 1;
  storage.insert({10, "a"});
  storage.insert({20, "b"});
  storage.end_of_life = 3;
  REQUIRE(storage.update());
  REQUIRE(!storage.update());
  REQUIRE(cnt == 2);
  sleep(2);
  storage.mark_active(20);
  REQUIRE(storage.update());
  REQUIRE(cnt == 4);
  storage.timeout = 10;

  // 20 was marked active, but 10, not so only 10 should be deleted
  // at end_of_life
  REQUIRE(storage.size() == 2);
  sleep(2);
  REQUIRE(storage.update());
  REQUIRE(storage.size() == 1);
  REQUIRE(!storage.contains(10));
  REQUIRE(storage.contains(20));

  // Force update (on next call to update()) even though timeout is set to much
  // later
  storage.mark_for_update(20);
  REQUIRE(cnt == 4);
  REQUIRE(storage.update());
  REQUIRE(cnt == 5);
  REQUIRE(!storage.update());
}

SCENARIO("We can convert storage to and from json") {
  active_storage<int, std::string> storage;
  storage.insert({10, "a"});
  storage.insert({20, "b"});
  storage.minimal_timeout = 1;
  storage.timeout = 2;
  storage.end_of_life = 3;
  nlohmann::json j = storage;
  REQUIRE(j["minimal_timeout"] == storage.minimal_timeout);
  REQUIRE(j["timeout"] == storage.timeout);
  REQUIRE(j["end_of_life"] == storage.end_of_life);
  REQUIRE(j["storage"].size() == storage.storage.size());

  active_storage<int, std::string> s2 = j;
  REQUIRE(j["minimal_timeout"] == s2.minimal_timeout);
  REQUIRE(j["timeout"] == s2.timeout);
  REQUIRE(j["end_of_life"] == s2.end_of_life);
  REQUIRE(j["storage"].size() == s2.storage.size());
}

struct state {
  int i = 0;
  state(int i, std::vector<state> kids = {}) : i(i), kids(kids) {}
  state() {}
  std::vector<state> kids;
};

SCENARIO("We can iterate on a flat_view_with_depth", "[flat_view]") {
  std::vector<state> sv{state(-2, {state(-1)}),
                        state(0, {state(1, {state(2, {})}),
                                  state(3, {state(4, {})}), state(5, {})})};
  constexpr auto dig_function = [](auto &s) -> auto{ return &s.kids; };
  auto view = flat_view_with_depth(sv, dig_function);

  auto it = view.begin();
  auto it2 = it;
  REQUIRE((*it).second.i == -2);
  REQUIRE((*it).first == 0);
  ++it;
  REQUIRE((*it).second.i == -1);
  REQUIRE((*it).first == 1);
  ++it;
  REQUIRE((*it).second.i == 0);
  REQUIRE((*it).first == 0);
  ++it;
  it2 = it;
  REQUIRE((*it).second.i == 1);
  REQUIRE((*it).first == 1);
}

SCENARIO("We can iterate skip digging a flat_view_with_depth", "[flat_view]") {
  std::vector<state> sv{state(-2, {state(-1)}),
                        state(0, {state(1, {state(2, {})}),
                                  state(3, {state(4, {})}), state(5, {})})};
  constexpr auto dig_function = [](auto &s) -> std::vector<state> * {
    return nullptr;
  };
  auto view = flat_view_with_depth(sv, dig_function);

  auto it = view.begin();
  REQUIRE((*it).second.i == -2);
  REQUIRE((*it).first == 0);
  ++it;
  REQUIRE((*it).second.i == 0);
  REQUIRE((*it).first == 0);
  ++it;
  REQUIRE((it == view.end()));
}

auto create_random_state_vector(size_t n, int seed) {
  std::uniform_real_distribution<double> runif(0.0, 1.0);
  std::default_random_engine generator(seed);
  size_t i = 0;
  size_t depth = 0;
  std::vector<std::pair<size_t, int>> test;
  std::vector<state> states;

  for (size_t i = 0; i < n; ++i) {
    state s;
    s.i = i;
    if (states.empty()) {
      states.push_back(s);
      test.push_back({i, depth});
      continue;
    }
    auto rnd = runif(generator);
    if (rnd < 0.1 && depth < 3) {
      ++depth;
    } else if (depth > 0 && rnd < 0.2) {
      --depth;
    }
    if (depth == 0)
      states.push_back(s);
    if (depth == 1)
      states.back().kids.push_back(s);
    if (depth == 2)
      states.back().kids.back().kids.push_back(s);
    if (depth == 3)
      states.back().kids.back().kids.back().kids.push_back(s);

    test.push_back({depth, i});
  }

  return std::pair(test, states);
}

SCENARIO("We can iterate on a seeded flat_view_with_depth", "[flat_view]") {
  auto rnd = create_random_state_vector(20, 1);
  auto view = flat_view_with_depth(
      rnd.second, [](auto &s) -> auto{ return &s.kids; });
  auto test_it = rnd.first.begin();
  for (auto s : view) {
    REQUIRE(s.first == test_it->first);
    REQUIRE(s.second.i == test_it->second);
    ++test_it;
  }
}

SCENARIO("We can iterate on a random flat_view_with_depth", "[flat_view]") {
  auto rnd = create_random_state_vector(20, time(0));
  auto view = flat_view_with_depth(
      rnd.second, [](auto &s) -> auto{ return &s.kids; });
  auto test_it = rnd.first.begin();
  for (auto s : view) {
    REQUIRE(s.first == test_it->first);
    REQUIRE(s.second.i == test_it->second);
    ++test_it;
  }
}

SCENARIO("flat view is an input iterator", "[flat_view]") {
  auto f = [](auto &s) -> std::vector<int> * { return nullptr; };
  static_assert(std::input_iterator<
                flat_view_iterator<std::vector<int>, decltype(f)>>);
  static_assert(std::bidirectional_iterator<
                flat_view_iterator<std::vector<int>, decltype(f)>>);
}
