#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt/config.hpp"
#include "rttt.hpp"
#include "rttt/twitter.hpp"

using namespace rttt;

SCENARIO("We can parse tweets by a user") {
  // Read "test/tweets_by_user.json"
  auto pFile = fopen("test/tweets_by_user.json", "r");
  // parse it to json
  auto j = nlohmann::json::parse(pFile);
  twitter::state state;
  REQUIRE(state.tweets.empty());
  REQUIRE(state.tweets_by_user_id.empty());
  twitter::parse_tweets_by_user_id(state, "0", j);
  REQUIRE(state.tweets.size() == 10);
  REQUIRE(state.tweets_by_user_id.size() == 1);

  auto tw = (*state.tweets.begin()).second.value;
  REQUIRE(tw.time > 0);
  REQUIRE(!tw.title.empty());
  REQUIRE(!tw.by.empty());

  // Test that they have all been marked as updated
  REQUIRE(state.tweets_by_user_id.begin()->second.need_update == false);
  for (auto &id : state.tweets_by_user_id.begin()->second.value) {
    REQUIRE(state.tweets.storage.at(id).need_update == false);
  }
}

SCENARIO("We can parse a conversation") {
  auto pFile = fopen("test/tweets_conversation.json", "r");
  // parse it to json
  auto j = nlohmann::json::parse(pFile);
  twitter::state state;
  REQUIRE(state.tweets.empty());
  REQUIRE(state.tweets_by_user_id.empty());
  // Add starting tweet
  std::string c_id = "1521551533787258882";
  state.tweets.insert({c_id, twitter::tweet()});
  state.tweets.at(c_id).text = "I am the mother tweet";

  // We are probably assuming that converversation id is same as tweet id?
  twitter::parse_tweets_by_conversation_id(state, c_id, j);
  REQUIRE(state.tweets.size() == 12);
  REQUIRE(state.tweets_by_conversation_id.size() == 1);
  REQUIRE(state.tweets_by_conversation_id.at(c_id).size() == 1);
  REQUIRE(state.tweets.at(c_id).kids.size() == 7);

  // We need to get the starting tweet as well?
  state.tweets.insert({c_id, twitter::tweet()});

  rttt::active_storage<std::string, view::item_state> ivs;
  auto v = twitter::comment_view(state, c_id, ivs);
  REQUIRE(!v.empty());
  // auto it = v.begin();
  // REQUIRE((*it).first == 1);
  auto i = 0;
  for (auto it = v.begin(); it != v.end(); ++it) {
    // std::cout << (*it).first << " " << (*it).second.text << std::endl;// << "
    // " << (*it).second.text << std::end;
    ++i;
  }
  REQUIRE(i == 8);
}

SCENARIO("We can extract first line to use it as a title") {
  std::string str = "a";
  auto split_first_line = rttt::text::split(str, "\n", 2);
  REQUIRE(split_first_line[0] == "a");
  REQUIRE(split_first_line.size() == 1);

  str = "a\nb\nc";
  split_first_line = rttt::text::split(str, "\n", 2);
  REQUIRE(split_first_line[0] == "a");
  REQUIRE(split_first_line.size() == 2);
  REQUIRE(split_first_line[1] == "b\nc");

  split_first_line = rttt::text::split(str, "\n", 3);
  REQUIRE(split_first_line[0] == "a");
  REQUIRE(split_first_line.size() == 3);
  REQUIRE(split_first_line[1] == "b");
  REQUIRE(split_first_line[2] == "c");

  str = "a\nb\nc\nd";
  split_first_line = rttt::text::split(str, "\n", 3);
  REQUIRE(split_first_line[0] == "a");
  REQUIRE(split_first_line.size() == 3);
  REQUIRE(split_first_line[2] == "c\nd");
}

SCENARIO("We can use the path API", "[.api]") {
  twitter::state state;
  twitter::setup(state, config::defaultConfig());

  std::string pth = "/tw/awesomekling";
  REQUIRE(twitter::is_valid_path(pth));

  auto path = twitter::parse_path(pth);
  REQUIRE(path.parts[1] == "awesomekling");
  size_t count = 5000;

  rttt::active_storage<std::string, view::item_state> dummy;
  REQUIRE(!twitter::try_path_view(state, path, dummy).has_value());
  while (state.tweets.size() <= 1 && count > 0) {
    twitter::retrieve_path_view(state, path);
    twitter::update(state);
    --count;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  REQUIRE(state.access_token);
  REQUIRE(twitter::try_user_id(state, "awesomekling"));
  REQUIRE(count > 0);
  REQUIRE(twitter::try_path_view(state, path, dummy).has_value());
  auto variant = twitter::try_path_view(state, path, dummy).value();
  std::visit(
      [](auto &v) {
        REQUIRE(!v.empty());
        /*std::cout << "TWEETS" << std::endl;
        for (auto pair : v | std::views::take(5)) {
          std::cout << pair.first << " " << pair.second.score << " "
                    << pair.second.descendants << " " << pair.second.by << " "
                    << pair.second.time << std::endl;
          std::cout << "Title: " << pair.second.title << std::endl;
          std::cout << pair.second.text << std::endl;
        }*/
      },
      variant);
  std::string c_id;
  std::visit([&c_id](auto &v) { c_id = (*v.begin()).second.id; }, variant);
  REQUIRE(!c_id.empty());
  pth += std::string("/comments/") + c_id;
  REQUIRE(twitter::is_valid_path(pth));
  path = twitter::parse_path(pth);
  REQUIRE(path.parts[1] == "awesomekling");
  REQUIRE(path.parts[3] == c_id);
  REQUIRE(path.mode == rttt::list_mode::comment);

  count = 5000;
  size_t no_tweets = state.tweets.size();
  while (no_tweets == state.tweets.size() && count > 0) {
    twitter::retrieve_path_view(state, path);
    twitter::update(state);
    --count;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  REQUIRE(count > 0);
  variant = twitter::try_path_view(state, path, dummy).value();
  std::visit(
      [](auto &v) {
        REQUIRE(!v.empty());
        /*std::cout << "CONVERSATION" << std::endl;
        for (auto pair : v | std::views::take(5)) {
          std::cout << pair.first << " " << pair.second.score << " "
                    << pair.second.descendants << " " << pair.second.by << " "
                    << pair.second.time << std::endl;
          std::cout << "Title: " << pair.second.title << std::endl;
          std::cout << pair.second.text << std::endl;
        }*/
      },
      variant);

  pth = "/hn/top";
  REQUIRE(!twitter::is_valid_path(pth));
}
