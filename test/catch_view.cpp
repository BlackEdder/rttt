#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"

#include "rttt/view.hpp"

using namespace rttt;

SCENARIO("We can convert item_view_state to and from json") {
  view::item_state vs(true, false);
  nlohmann::json j = vs;
  REQUIRE(j["collapsed"] == true);
  REQUIRE(j["read"] == false);

  vs.collapsed = false;
  vs.read = true;
  vs = j;
  REQUIRE(vs.collapsed == true);
  REQUIRE(vs.read == false);
}

SCENARIO("Bitwise works on our layout enum") {
  using namespace view;
  auto lay = layout::base;
  REQUIRE(lay == layout::base);
  
  REQUIRE(lay != layout::text);
  REQUIRE(lay != layout::numbering);

  lay |= layout::text;
  REQUIRE(lay == layout::text);
  REQUIRE(lay != layout::numbering);

  lay |= layout::numbering;
  REQUIRE(lay == layout::text);
  REQUIRE(lay == layout::numbering);

  lay ^= layout::numbering;
  REQUIRE(lay == layout::text);
  REQUIRE(lay != layout::numbering);

  lay = layout::numbering;
  REQUIRE(lay != layout::text);
  REQUIRE(lay == layout::numbering);
}
