#pragma once

#include <locale.h>
#include <stdio.h>
#include <time.h>

#include <chrono>
#include <deque>
#include <fstream>
#include <optional>
#include <source_location>
#include <string>

#include "fmt/chrono.h"
#include "fmt/format.h"

namespace logger {

namespace {
// Anonymous namespace makes this private to this namespace (basically a global)
std::deque<std::string> queue;
std::optional<std::string> maybe_file;
} // namespace

void write_to_log_file(const std::string &message) {
  if (maybe_file.has_value()) {
    std::ofstream log_file;
    log_file.open(maybe_file.value(), std::ios_base::app);
    log_file << message << std::endl;
    log_file.close();
  }
}

inline void push_back(const std::string &message) {
  time_t t =
      std::chrono::system_clock::to_time_t(std::chrono::system_clock().now());

  std::string line =
      fmt::format("[{:%H:%M:%S}] {}", fmt::localtime(t), message);

  write_to_log_file(line);
  queue.push_back(line);
}

template <typename... Args>
void push(const std::string &fmt_string, Args &&...args) {
  std::string msg =
      fmt::format(fmt::runtime(fmt_string), std::forward<Args>(args)...);
  logger::push_back(msg);
}

template <typename T>
inline void log_ifnot(T pass, const std::source_location location =
                                  std::source_location::current()) {
  if (!pass) {
    std::time_t t =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock().now());

    std::string message = fmt::format(
        "[{:%H:%M:%S}] ERROR: Assertion failed at line {} in file {}.",
        fmt::localtime(t), location.line(), location.file_name());
    write_to_log_file(message);
    queue.push_back(message);
  }
}

inline std::string at(const size_t i) { return queue.at(i); }

inline size_t size() { return queue.size(); }

inline void pop_front() { queue.pop_front(); }

void to_file(const std::string &filename) { maybe_file = filename; }

} // namespace logger
