#pragma once

#include <algorithm>
#include <optional>
#include <string>
#include <variant>

// Should with always return itself? Otherwise use transform?
// If the latter then we should separate try (from try_with):
// try(with()), try(transform)
// visit(with()), visit(transform)
template <typename F> struct with {
  F func;
  with(F &&f) : func(f) {}
  with(F &f) : func(f) {}

  template <typename T> auto operator()(T &value) const { return func(value); }
  template <typename T> auto operator()(T &&value) const { return func(value); }

  /*template <typename T>
  auto operator()(T &value) const -> decltype(func(std::forward<T>(value)))
  requires( std::is_invocable<F, decltype(std::forward<T>(value))>::value) {
    return func(std::forward<T>(value));
  }*/

  /*
    template <typename T>
    auto operator()(T &&value) const -> decltype(func(std::forward<T>(value)))
    requires( std::is_invocable<F, decltype(std::forward<T>(value))>::value) {
      return func(std::forward<T>(value));
    }*/
};

template <typename F, typename G>
auto operator|(F &f, with<G> g) requires(!std::is_invocable<F>::value) {
  return g(f);
}

template <typename F, typename G>
auto operator|(F &&f, with<G> g) requires(!std::is_invocable<F>::value) {
  return g(f);
}

/*template <typename F, typename G> auto operator|(with<F> f, with<G> g) {
  return with([f, g](auto &value) {
    return g(f(value));
  });
}*/

// Not yet optional
template <typename T> struct add_optionality_t {
  using type = std::optional<T>;
};

// Already optional
template <typename T> struct add_optionality_t<std::optional<T>> {
  using type = std::optional<T>;
};

template <typename T>
using add_optionality = typename add_optionality_t<T>::type;

template <typename F> struct try_with {
  F func;
  try_with(F &&f) : func(f) {}
  try_with(F &f) : func(f) {}

  template <typename T>
  auto operator()(std::optional<T> &value)
      -> add_optionality<decltype(func(std::forward<T>(value.value())))>
  requires(
      std::is_invocable<F, decltype(std::forward<T>(value.value()))>::value) {
    if (value.has_value()) {
      return func(std::forward<T>(value.value()));
    }
    return std::nullopt;
  }

  template <typename T>
  auto operator()(std::optional<T> &&value)
      -> add_optionality<decltype(func(std::forward<T>(value.value())))>
  requires(
      std::is_invocable<F, decltype(std::forward<T>(value.value()))>::value) {
    if (value.has_value()) {
      return func(std::forward<T>(value.value()));
    }
    return std::nullopt;
  }
};

template <typename F, typename G>
auto operator|(F &f, try_with<G> g) requires(!std::is_invocable<F>::value) {
  return g(f);
}

template <typename F, typename G>
auto operator|(F &&f, try_with<G> g) requires(!std::is_invocable<F>::value) {
  return g(std::move(f));
}

template <typename F> struct visit_with {
  F func;
  visit_with(F &&f) : func(f) {}
  visit_with(F &f) : func(f) {}

  template <typename T> auto operator()(T &value) {
    return std::visit(func, std::forward<T>(value));
  }

  template <typename T> auto operator()(T &&value) {
    return std::visit(func, std::forward<T>(value));
  }
};

template <typename F, typename G>
auto operator|(F &f, visit_with<G> g) requires(!std::is_invocable<F>::value) {
  return g(f);
}

template <typename F, typename G>
auto operator|(F &&f, visit_with<G> g) requires(!std::is_invocable<F>::value) {
  return g(std::move(f));
}

/// Collect a range into a specified type (e.g. std::vector<int>)
template <typename T> auto collect() {
  return with([](auto &range) {
    T into;
    std::ranges::copy(range.begin(), range.end(), std::back_inserter(into));
    return into;
  });
}

/// Turn variable into optional, if variable is empty, then optional is nullopt
template <typename T> std::optional<T> empty_as_optional() {
  return with<T>([](auto &content) {
    if (!content.empty())
      return content;
    return std::nullopt;
  });
}
