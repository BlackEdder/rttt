#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <random>

#include "nlohmann/json.hpp"

#include "functional.hpp"
#include "prompt.hpp"

namespace rttt {

namespace config {

inline std::string random_string(size_t size) {
  const std::string characters =
      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  std::default_random_engine generator(time(0));
  std::uniform_int_distribution<> distribution(0, characters.size() - 1);

  std::string random_string;
  random_string.resize(size);

  for (std::size_t i = 0; i < size; ++i) {
    random_string[i] = characters[distribution(generator)];
  }

  return random_string;
}

inline std::filesystem::path getPath() {
  auto path_char = std::getenv("XDG_CONFIG_HOME");
  if (path_char != NULL)
    return std::filesystem::path(std::filesystem::path(path_char) / "rttt");
  std::filesystem::path path =
      std::filesystem::path(std::getenv("HOME")) / ".config/rttt";
  return std::filesystem::absolute(path);
}

inline std::filesystem::path getFilePath() { return getPath() / "config.json"; }

// TODO: Move to thing.hpp (and source specific configuration to their files)
nlohmann::json defaultConfig() {
  nlohmann::json config;
  config["reddit"]["client_id"] = "8yDBiibHONI95SeMWLZspg";
  config["reddit"]["device_id"] = rttt::config::random_string(25);
  config["reddit"]["refresh_token"] = "";
  config["twitter"]["api_key"] = "FEZfkDOci69v51iVgkUsfYZuK";
  config["twitter"]["api_secret_key"] =
      "gJmmYEh4SWRvg2V4kRWbMtgbXDrpgKYV2EIFFtWJkC5pDuH0ra";
  config["rss"]["opml_file"] = rttt::config::getPath() / "subscriptions.opml";
  config["open_command"] = "xdg-open";
  config["start_path"] = "/hn/top";
  return config;
}

inline bool save(const nlohmann::json &json) {
  std::ofstream file;
  file.open(getFilePath().c_str());
  file << json.dump(2) << std::endl;
  file.close();
  return true;
}

inline nlohmann::json load() {
  std::filesystem::path fn = "config.json";
  auto path = getPath();
  nlohmann::json config;
  if (!std::filesystem::exists(path)) {
    std::filesystem::create_directory(path);
  }
  auto fpath = getFilePath();
  if (!std::filesystem::exists(fpath)) {
    config = defaultConfig();
    save(config);
  } else {
    FILE *pFile;
    pFile = fopen(fpath.c_str(), "r");
    auto saved_config = nlohmann::json::parse(pFile);

    // Make sure config contains all expected values
    config = defaultConfig();
    // Overwrite with saved values
    config.update(saved_config);
  }
  return config;
}

inline std::filesystem::path get_data_path() {
  auto path_char = std::getenv("XDG_DATA_HOME");
  if (path_char != NULL)
    return std::filesystem::path(std::filesystem::path(path_char) / "rttt");
  std::filesystem::path path =
      std::filesystem::path(std::getenv("HOME")) / ".local/share/rttt";
  return std::filesystem::absolute(path);
}

template <typename T>
nlohmann::json load_data_from_file_or(const T &default_states,
                                      const std::filesystem::path &fn) {
  auto path = get_data_path();
  if (!std::filesystem::exists(path)) {
    std::filesystem::create_directory(path);
  }
  auto fpath = path / fn;

  nlohmann::json data;

  if (!std::filesystem::exists(fpath)) {
    nlohmann::json j = default_states;
    return j;
  }

  FILE *p_file;
  p_file = fopen(fpath.c_str(), "r");
  return nlohmann::json::parse(p_file);
}

bool save_to_data_path(const nlohmann::json &json,
                       const std::filesystem::path &fn) {
  auto path = get_data_path() / fn;
  std::ofstream file;
  file.open(path);
  file << json.dump() << std::endl;
  file.close();
  return true;
}

template <typename T>
nlohmann::json load_item_view_states_or(const T &default_states) {
  return load_data_from_file_or(default_states, "item_view_state.json");
}

bool save_item_view_states(const nlohmann::json &json) {
  return save_to_data_path(json, "item_view_state.json");
}

auto load_prompt_state_or(rttt::prompt::state &default_state) {
  return with([&default_state](rttt::prompt::state &state) -> rttt::prompt::state & {
    state = load_data_from_file_or(default_state, "prompt_state.json");
    return state;
  });
}

bool save_prompt_state(const nlohmann::json &json) {
  return save_to_data_path(json, "prompt_state.json");
}

} // namespace config
} // namespace rttt
