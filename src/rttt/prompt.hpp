#pragma once

#include <cstddef>
#include <regex>
#include <set>
#include <string>
#include <tuple>
#include <vector>

#include <nlohmann/json.hpp>

namespace rttt {
namespace prompt {
struct compare_lower_case {
  bool operator()(const std::string &a, const std::string &b) const {
    for (size_t i = 0; i < std::min(a.size(), b.size()); ++i) {
      auto la = std::tolower(a[i]);
      auto lb = std::tolower(b[i]);
      if (la == lb)
        continue;
      return la < lb;
    }
    return a.size() < b.size();
  }
};

struct completer {
  std::set<std::string, prompt::compare_lower_case> matches;

  std::string_view next(const std::string &partial) {
    static decltype(matches)::iterator it;
    std::regex re("^" + partial, std::regex::icase);
    // Check for size in case new matches have been added
    if (std::get<0>(current_state) != partial ||
        std::get<2>(current_state) != matches.size()) {
      it = std::find_if(
          matches.begin(), matches.end(),
          [&re](std::string match) { return std::regex_search(match, re); });
      current_state =
          std::tuple<std::string, decltype(matches)::iterator, size_t>{
              partial, it, matches.size()};
      if (it == matches.end())
        return {};
      return (*it);
    } else {
      ++it;
      if (it == matches.end() || !std::regex_search((*it), re)) {
        // Restart at the beginning
        it = std::get<1>(current_state);
        if (it == matches.end())
          return {};
        return (*it);
      }
      return (*it);
    }
    return {};
  }

private:
  std::tuple<std::string, decltype(matches)::iterator, size_t> current_state;
};

void to_json(nlohmann::json &j, const completer &s) {
  j = nlohmann::json{{"matches", s.matches}};
}

void from_json(const nlohmann::json &j, completer &s) {
  j.at("matches").get_to(s.matches);
}

struct state {
  std::vector<std::string> history;
  size_t history_id = 0;
  prompt::completer completer;
};

state push(state &&state, const std::string &path) {
  auto it = std::remove_if(
      state.history.begin(), state.history.end(),
      [&path](const auto &element) -> bool { return element == path; });
  state.history.erase(it, state.history.end());
  state.history.push_back(path);
  state.history_id = state.history.size();
  state.completer.matches.insert(path);
  return std::move(state);
}

void to_json(nlohmann::json &j, const state &s) {
  j = nlohmann::json{{"history", s.history},
                     {"history_id", s.history_id},
                     {"completer", s.completer}};
}

void from_json(const nlohmann::json &j, state &s) {
  j.at("history").get_to(s.history);
  j.at("history_id").get_to(s.history_id);
  j.at("completer").get_to(s.completer);
}

void arrow(state &state, std::string &input, bool up = true) {
  if (up && state.history_id > 0) {
    --state.history_id;
  } else if (!up && state.history_id < state.history.size() - 1) {
    ++state.history_id;
  }

  if (!state.history.empty()) {
    input = state.history[state.history_id];
  }
}

void tab_completion(state &state, std::string &input, int position) {
  auto m = state.completer.next(input.substr(0, position));
  if (!m.empty()) {
    input = m;
  }
}
} // namespace prompt
} // namespace rttt
