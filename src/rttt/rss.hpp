#pragma once

#include <algorithm>
#include <map>
#include <set>

#include "pugixml.hpp"

#include "rttt.hpp"
#include "rttt/command.hpp"
#include "rttt/functional.hpp"
#include "rttt/future.hpp"
#include "rttt/item.hpp"
#include "rttt/logger.hpp"
#include "rttt/request.hpp"
#include "rttt/text.hpp"
#include "rttt/view.hpp"

namespace rttt {
namespace rss {

std::wstring empty_opml = LR"(
<?xml version="1.0" encoding="UTF-8"?>
<opml version="2.0">
  <head>
    <title>Subscriptions</title>
  </head>
  <body>
  </body>
</opml>
)";

auto compare_stories = [](const rss::Story &a, const rss::Story &b) {
  if (a.time == b.time)
    return (a.id > b.id);
  return (a.time > b.time);
};
using story_set = std::set<rss::Story, decltype(compare_stories)>;

inline std::map<std::string, std::string> parse_opml(pugi::xml_node doc) {
  std::map<std::string, std::string> urls;
  for (auto &child : doc.children()) {
    auto attrurl = child.attribute("xmlUrl");
    auto attrtitle = child.attribute("title");
    if (attrtitle && attrurl) {
      urls.insert({attrtitle.value(), attrurl.value()});
    } else {
      auto nested = parse_opml(child);
      urls.merge(nested);
    }
  }
  return urls;
}

inline std::tm parse_pubdate(std::string time_string) {
  // parse_pubdate("Wed, 24 Nov 2021 13:46:56 +0000");
  std::tm time = {};
  std::regex e("(\\d+) ([A-z]{3}) (\\d{4})");
  std::smatch sm;
  std::regex_search(time_string, sm, e);
  if (sm.size() > 1) {
    time.tm_mday = std::stoi(sm[1]);
    if (sm[2] == "Dec") {
      time.tm_mon = 11;
    } else if (sm[2] == "Nov") {
      time.tm_mon = 10;
    } else if (sm[2] == "Oct") {
      time.tm_mon = 9;
    } else if (sm[2] == "Sep") {
      time.tm_mon = 8;
    } else if (sm[2] == "Aug") {
      time.tm_mon = 7;
    } else if (sm[2] == "Jul") {
      time.tm_mon = 6;
    } else if (sm[2] == "Jun") {
      time.tm_mon = 5;
    } else if (sm[2] == "May") {
      time.tm_mon = 4;
    } else if (sm[2] == "Apr") {
      time.tm_mon = 3;
    } else if (sm[2] == "Mar") {
      time.tm_mon = 2;
    } else if (sm[2] == "Feb") {
      time.tm_mon = 1;
    } else if (sm[2] == "Jan") {
      time.tm_mon = 0;
    } else {
      logger::log_ifnot(false);
    }
    time.tm_year = std::stoi(sm[3]) - 1900;
  }

  // Try matching hour and minute
  e = std::regex("\\d+ [A-z]{3} \\d{4} (\\d+):(\\d+)");
  std::regex_search(time_string, sm, e);
  if (sm.size() > 1) {
    time.tm_hour = std::stoi(sm[1]);
    time.tm_min = std::stoi(sm[2]);
  }
  return time;
}

std::string find_best_link(const pugi::xml_node &entry) {
  std::string url = {};
  for (auto lnk = entry.child("link"); lnk; lnk = lnk.next_sibling("link")) {
    if (url.empty()) {
      if (lnk.attribute("href")) {
        url = lnk.attribute("href").value();
      } else {
        url = lnk.text().get();
      }
    } else if (lnk.attribute("rel") &&
               std::string("alternate") == lnk.attribute("rel").value()) {
      url = lnk.attribute("href").value();
    }
  }
  return url;
}

rss::story_set parse_atom(const pugi::xml_document &doc,
                          const std::optional<std::string> &key) {
  auto feed = doc.child("feed");
  auto by = feed.child("author").child("name").text().get();
  auto blog = feed.child("title").text().get();
  // Parse all the entries, get url title, by, time and text
  rss::story_set stories;
  for (pugi::xml_node entry = feed.child("entry"); entry;
       entry = entry.next_sibling("entry")) {
    rss::Story story;
    story.title = entry.child("title").text().get();
    story.text = rttt::parse_html(entry.child("content").text().get());
    story.url = find_best_link(entry);
    story.by = by;

    story.domain = blog;
    story.key = story.domain;
    if (key)
      story.key = key.value();

    auto time_tm = parse_time(entry.child("updated").text().get());
    story.time = std::mktime(&time_tm);
    story.id = std::hash<std::string>{}(story.key + story.title +
                                        std::to_string(story.time));
    stories.insert(story);
  }
  return stories;
}

rss::story_set parse_rss(const pugi::xml_document &doc,
                         const std::optional<std::string> &key) {
  auto channel = doc.child("rss").child("channel");
  auto feed = doc.child("rss").child("channel");
  if (!channel) {
    feed = doc.child("rdf:RDF");
    channel = feed.child("channel");
  }
  auto blog = channel.child("title").text().get();
  // Parse all the entries, get url title, by, time and text
  rss::story_set stories;
  for (pugi::xml_node entry = feed.child("item"); entry;
       entry = entry.next_sibling("item")) {
    rss::Story story;
    story.title = entry.child("title").text().get();
    story.text = rttt::parse_html(entry.child("description").text().get());
    story.url = find_best_link(entry);
    story.by = entry.child("dc:creator").text().get();
    story.domain = blog;
    story.key = story.domain;
    if (key)
      story.key = key.value();

    if (entry.child("pubDate")) {
      auto time_tm = parse_pubdate(entry.child("pubDate").text().get());
      story.time = std::mktime(&time_tm);
    } else if (entry.child("lastBuildDate")) {
      auto time_tm = parse_pubdate(entry.child("lastBuildDate").text().get());
      story.time = std::mktime(&time_tm);
    } else if (entry.child("dc:date")) {
      auto time_tm = parse_time(entry.child("dc:date").text().get());
      story.time = std::mktime(&time_tm);
    } else {
      logger::log_ifnot(false);
    }

    story.id = std::hash<std::string>{}(story.key + story.title +
                                        std::to_string(story.time));
    stories.insert(story);
  }
  return stories;
}

inline bool is_rss(const pugi::xml_document &doc) {
  return doc.child("rss") || doc.child("rdf:RDF");
}

inline rss::story_set parse_feed(const std::string &content,
                                 const std::string &key) {
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_string(content.c_str());
  if (!result)
    logger::push("RSS: unable to load content");
  if (is_rss(doc))
    return parse_rss(doc, key);
  else
    return parse_atom(doc, key);
}

struct state {
  std::map<std::string, std::string> feed_urls;
  rttt::active_storage<std::string, rss::story_set> items;

  request::State<std::string> requests;
  future::future_wait_list<cpr::Response> wait_list;

  rss::story_set item_cache;

  state() = default;

  bool empty() { return feed_urls.empty(); }

  bool update() {
    items.update();
    requests = request::update(std::move(requests));
    bool updated = future::update(this->wait_list);
    auto maybe_receive = request::try_pop_and_retrieve(requests);
    while (maybe_receive) {
      const auto &key = maybe_receive.value().first;
      updated = true;
      items.mark_updated(key);
      // parse as below
      auto v = parse_feed(maybe_receive.value().second, key);
      if (v.empty()) {
        logger::push("No items found in: {}", key);
        maybe_receive = request::try_pop_and_retrieve(requests);
        continue;
      }

      logger::log_ifnot(this->items.contains(key));
      // FIXME use the add_to function below. Probably will need to move this-
      // function outside of the struct
      for (auto &&item : v) {
        auto pair = item_cache.insert(item);
        // All other items seem to already exist
        if (!pair.second)
          break;
      }
      this->items.at(key) = std::move(v);
      maybe_receive = request::try_pop_and_retrieve(requests);
    }
    return updated;
  }
};

auto &get_items(const rss::state &state, const rttt::Path &path) {
  if (path.mode == rttt::list_mode::feed && path.parts.size() > 1) {
    logger::log_ifnot(state.items.contains(path.parts[1]));
    return state.items.at(path.parts[1]);
  }
  return state.item_cache;
}

auto try_domain() {
  return with([](const rss::story_set &stories) -> std::optional<std::string> {
    if (stories.empty()) {
      return std::nullopt;
    } else {
      return stories.begin()->domain;
    }
  });
}

auto insert_into_opml(const std::string &opml_file, const std::string &url) {
  return with([&opml_file, &url](auto &stories) -> decltype(stories) & {
    stories | try_domain() |
        try_with([&opml_file, &url](const std::string &domain) {
          pugi::xml_document doc;
          pugi::xml_parse_result result = doc.load_file(opml_file.c_str());
          logger::log_ifnot(result);
          // bdy.print(std::cout);
          auto outl = doc.child("opml").child("body").append_child("outline");
          outl.append_attribute("title") = domain.c_str();
          outl.append_attribute("text") = domain.c_str();
          outl.append_attribute("xmlUrl") = url.c_str();
          doc.save_file(opml_file.c_str());
          return true;
        });
    return stories;
  });
}

auto add_to(rss::state &state) {
  return with([&state](auto &stories) -> decltype(stories) & {
    auto maybe_domain = stories | try_domain();
    if (maybe_domain.has_value()) {
      auto domain = maybe_domain.value();
      for (auto &&item : stories) {
        auto pair = state.item_cache.insert(item);
        // All other items seem to already exist
        if (!pair.second)
          break;
      }
      if (state.items.contains(domain))
        state.items.at(domain) = stories;
      else
        state.items.insert({domain, stories});
    }
    return stories;
  });
}

auto rss_feed_add(rss::state &state, cpr::Url url,
                  const std::string &opml_file) {
  return std::pair{
      cpr::GetAsync(url), [opml_file, url, &state](cpr::Response &&response) {
        logger::log_ifnot(response.status_code == 200);
        auto stories = rss::parse_feed(response.text, {});
        stories | insert_into_opml(opml_file, url.str()) | add_to(state);
        stories | try_domain() |
            try_with([&state, &url](const std::string domain) {
              state.feed_urls.insert({domain, url.str()});
              return domain;
            });
      }};
}

auto setup(state &state, nlohmann::json config) {
  std::filesystem::path opml_file = config["rss"]["opml_file"];
  if (!std::filesystem::exists(opml_file)) {
    std::wofstream file(opml_file);
    file << empty_opml;
    file.close();
  }

  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_file(opml_file.c_str());
  if (!result)
    logger::push("RSS: unable to load opml file");
  state.feed_urls = parse_opml(doc);

  state.items =
      rttt::active_storage<std::string, rss::story_set>(15 * 60, 60, 0);
  for (auto &pair : state.feed_urls)
    state.items.insert({pair.first, {}});

  state.items.on_timeout = [&state](const std::string &key,
                                    [[maybe_unused]] auto &value) {
    state.requests =
        request::push(std::move(state.requests), state.feed_urls.at(key), key);
  };
}

auto update(state &state) { return state.update(); }

bool is_valid_path(std::string_view name) {
  auto v = rttt::text::split(name, "/");
  return (v.size() > 0 && v[0] == "rss");
}

bool is_valid_path(const rttt::Path &path) {
  // TODO: should we rely on path.type if it is present?
  return (path.parts.size() == 1 || path.parts.size() == 2) &&
         path.parts[0] == "rss";
}

Path parse_path(std::string_view name) {
  auto path = rttt::parse_path(name);
  path.basename = "/rss";
  if (path.parts.size() > 1)
    path.mode = list_mode::feed;
  return path;
}

auto retrieve_path_view([[maybe_unused]] const state &state,
                        [[maybe_unused]] const rttt::Path &path) {
  return;
}

auto try_path_view_helper(const state &state, const rttt::Path &path) {
  auto &vec = rttt::rss::get_items(state, path);
  auto v = vec | std::views::transform(
                     [](const auto &item) { return std::pair(0, item); });
  return v;
}

auto try_path_view(
    const state &state, const rttt::Path &path,
    [[maybe_unused]] const rttt::active_storage<std::string, view::item_state>
        &item_view_states) {
  logger::log_ifnot(rss::is_valid_path(path));
  std::optional<std::variant<decltype(try_path_view_helper(state, {}))>> opt(
      try_path_view_helper(state, path));
  return opt;
}

auto completions(const state &state) {
  return with([&state](auto &set) -> decltype(set) & {
    for (auto &pair : state.feed_urls)
      set.insert("/rss/" + pair.first);
    return set;
  });
}

auto commands(state &state) {
  return with([&state](auto &commands) -> decltype(commands) & {
    commands | command::push(
                   "rss feed add",
                   command::command() |
                       command::function([&state](std::string_view url) {
                         std::string opml_filename =
                             config::load()["rss"]["opml_file"];
                         logger::push("add feed {} to opml file {}", url,
                                      opml_filename);
                         state.wait_list.push_back(rss_feed_add(
                             state, cpr::Url(std::string(url)), opml_filename));
                         return true;
                       }) |
                       command::help(" <url>: Add feed to rss reader"));
    return commands;
  });
}
} // namespace rss
} // namespace rttt
