#pragma once

#include <memory>
#include <vector>

namespace rttt {
namespace timeline {

template<typename T>
struct timeline_state {
  std::vector<std::string> paths = {"/r/front", "/rss/Mike Blumenkrantz",
                                    "/hn/top", "/r/funny"};
  T *thing_state;
};

template<typename T>
using state = std::shared_ptr<timeline_state<T>>;

}; // namespace timeline
}; // namespace rttt
