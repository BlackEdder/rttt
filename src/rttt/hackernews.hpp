#pragma once

#include <map>
#include <queue>
#include <ranges>
#include <set>
#include <string>
#include <unordered_set>

#include "cpr/cpr.h"
#include "fmt/format.h"
#include "nlohmann/json.hpp"

#include "rttt.hpp"
#include "rttt/item.hpp"
#include "rttt/logger.hpp"
#include "rttt/request.hpp"
#include "rttt/storage.hpp"
#include "rttt/text.hpp"
#include "rttt/view.hpp"

namespace rttt {
namespace hackernews {

using ItemData = std::map<std::string, std::string>;

namespace {
rttt::request::State<std::string> pathRequestCache;
rttt::request::State<ItemId> itemRequestCache;
} // namespace

struct Job {
  std::string by = "";
  ItemId id = 0;
  int score = 0;
  uint64_t time = 0;
  std::string title = "";
  std::string url = "";
  std::string domain = "";
};

struct Poll {
  std::string by = "";
  int descendants = 0;
  ItemId id = 0;
  ItemIds kids;
  ItemIds parts;
  int score = 0;
  uint64_t time = 0;
  std::string text = "";
  std::string title = "";
};

struct PollOpt {
  std::string by = "";
  ItemId id = 0;
  ItemId poll = 0;
  int score = 0;
  uint64_t time = 0;
  std::string text = "";
};

using ItemContainer = rttt::active_storage<int, rttt::ItemVariant>;

template <typename T>
T safe_parse(const nlohmann::json &json, std::string key) {
  if (json.contains(key))
    return json[key].get<T>();
  return T();
}

template <typename T> T parseData(const nlohmann::json &json) {
  T data;

  if constexpr (std::is_same<T, Comment>::value ||
                std::is_same<T, Story>::value || std::is_same<T, Job>::value ||
                std::is_same<T, Poll>::value ||
                std::is_same<T, PollOpt>::value) {
    data.by = safe_parse<std::string>(json, "by");
    data.id = safe_parse<int>(json, "id");
    data.time = safe_parse<size_t>(json, "time");
  }
  if constexpr (std::is_same<T, Comment>::value ||
                std::is_same<T, Story>::value || std::is_same<T, Poll>::value) {
    data.kids = safe_parse<std::vector<int>>(json, "kids");
    data.text = rttt::parse_html(safe_parse<std::string>(json, "text"));
  }
  if constexpr (std::is_same<T, Story>::value || std::is_same<T, Job>::value ||
                std::is_same<T, Poll>::value) {
    data.score = safe_parse<int>(json, "score");
    data.title = rttt::parse_html(safe_parse<std::string>(json, "title"));
  }
  if constexpr (std::is_same<T, Story>::value || std::is_same<T, Job>::value) {
    data.url = safe_parse<std::string>(json, "url");
    int slash = 0;
    for (auto &ch : data.url) {
      if (ch == '/') {
        ++slash;
        continue;
      }
      if (slash > 2)
        break;
      if (slash > 1)
        data.domain += ch;
    }
  }
  if constexpr (std::is_same<T, Comment>::value) {
    data.parent = safe_parse<int>(json, "parent");
  }
  if constexpr (std::is_same<T, Story>::value || std::is_same<T, Poll>::value) {
    data.descendants = safe_parse<int>(json, "descendants");
  }
  if constexpr (std::is_same<T, Poll>::value) {
    data.kids = safe_parse<std::vector<int>>(json, "kids");
  }
  if constexpr (std::is_same<T, PollOpt>::value) {
    data.score = safe_parse<int>(json, "score");
    data.text = rttt::parse_html(safe_parse<std::string>(json, "text"));
    data.poll = safe_parse<int>(json, "poll");
  }
  return data;
}

using URI = std::string;
// https://github.com/HackerNews/API

namespace {
const URI kAPIItem = "https://hacker-news.firebaseio.com/v0/item/";
const URI kAPITopStories =
    "https://hacker-news.firebaseio.com/v0/topstories.json";
const URI kAPINewStories =
    "https://hacker-news.firebaseio.com/v0/newstories.json";
const URI kAPIAskStories =
    "https://hacker-news.firebaseio.com/v0/askstories.json";
const URI kAPIShowStories =
    "https://hacker-news.firebaseio.com/v0/showstories.json";
const URI kAPIJobStories =
    "https://hacker-news.firebaseio.com/v0/jobstories.json";
const URI kAPIUpdates = "https://hacker-news.firebaseio.com/v0/updates.json";
} // namespace

inline URI getItemURI(ItemId id) {
  return kAPIItem + std::to_string(id) + ".json";
}

inline ItemIds getChangedItemsIds() {
  ItemIds data;
  auto string_opt =
      rttt::request::try_retrieve(pathRequestCache, std::string("/hn/updates"));
  if (string_opt.has_value()) {
    nlohmann::json json = nlohmann::json::parse(string_opt.value());
    data = json["items"].get<ItemIds>();
  }
  return data;
}

enum class ItemType : int {
  Unknown,
  Story,
  Comment,
  Job,
  Poll,
  PollOpt,
};

inline ItemType get_item_type(const nlohmann::json &json) {
  if (!json.contains("type"))
    return ItemType::Unknown;

  const std::string &strType = json["type"];

  if (strType == "story")
    return ItemType::Story;
  if (strType == "comment")
    return ItemType::Comment;
  if (strType == "job")
    return ItemType::Job;
  if (strType == "poll")
    return ItemType::Poll;
  if (strType == "pollopt")
    return ItemType::PollOpt;

  return ItemType::Unknown;
}

struct state {
  /**
   * Update the content
   *
   * @param toRefresh Current visible ids
   */
  bool update() {
    /*
    My understanding of how this works:

    activeItems are the currently visible stories
    Every 30 seconds we update the changed/updated list based on kAPIUpdates
    (updates.json) If changed && part of toRefresh then needUpdate and
    needRequest = true
    */
    bool updated = false;

    // Every 30 seconds check for updated item ids
    if (rttt::timeout(lastUpdatePoll_s, 30)) {
      pathRequestCache = rttt::request::push(std::move(pathRequestCache),
                                             hackernews::kAPIUpdates,
                                             std::string("/hn/updates"));

      lastUpdatePoll_s = rttt::current_time();
    }

    {
      auto ids = hackernews::getChangedItemsIds();
      for (auto id : ids) {
        if (!items.contains(id))
          continue;
        items.mark_for_update(id);
        items.mark_active(id);
      }
    }

    // Note that this has to happen after getChangedItemsIds, because that one
    // will empty the "/hn/updates" which requires special attention
    auto maybe_receive_path =
        rttt::request::try_pop_and_retrieve(pathRequestCache);
    while (maybe_receive_path.has_value()) {
      auto &pair = maybe_receive_path.value();
      if (pair.first == "/hn/updates") {
        logger::log_ifnot(false);
      }

      if (!story_ids_by_path.contains(pair.first))
        continue;

      ItemIds ids = nlohmann::json::parse(pair.second);
      story_ids_by_path.at(pair.first) = std::move(ids);
      updated = true;
      maybe_receive_path =
          rttt::request::try_pop_and_retrieve(pathRequestCache);
    }

    auto maybe_receive = rttt::request::try_pop_and_retrieve(itemRequestCache);
    while (maybe_receive.has_value()) {
      if (!items.contains(maybe_receive.value().first)) {
        maybe_receive = rttt::request::try_pop_and_retrieve(itemRequestCache);
        continue;
      }

      auto json_string = maybe_receive.value().second;

      const auto json = nlohmann::json::parse(json_string);
      const auto type = get_item_type(json);
      switch (type) {
      case ItemType::Story: {
        logger::log_ifnot(items.contains(maybe_receive.value().first));
        items.at(maybe_receive.value().first) =
            hackernews::parseData<hackernews::Story>(json);
        items.mark_updated(maybe_receive.value().first);
        updated = true;
      } break;
      case ItemType::Comment: {
        logger::log_ifnot(items.contains(maybe_receive.value().first));
        items.at(maybe_receive.value().first) =
            hackernews::parseData<hackernews::Comment>(json);
        items.mark_updated(maybe_receive.value().first);
        updated = true;
      } break;
        // Unsupported for now
      case ItemType::Job:
      case ItemType::Poll:
      case ItemType::PollOpt:
      case ItemType::Unknown: {
        logger::log_ifnot(items.contains(maybe_receive.value().first));
        items.at(maybe_receive.value().first) = rttt::unknown_type();
        items.mark_updated(maybe_receive.value().first);
      } break;
      };

      maybe_receive = rttt::request::try_pop_and_retrieve(itemRequestCache);
    }

    // Submit requests for active items
    story_ids_by_path.update();
    comment_view_store.update();
    items.update();

    // Process any new received requests
    itemRequestCache = rttt::request::update(std::move(itemRequestCache));
    pathRequestCache = rttt::request::update(std::move(pathRequestCache));

    return updated;
  }

  // Store for comment_views
  active_storage<int, std::vector<int>> comment_view_store =
      active_storage<int, std::vector<int>>(300, 10, 3600);

  active_storage<std::string, std::vector<int>> story_ids_by_path =
      active_storage<std::string, std::vector<int>>(300, 10, 1000);

  std::map<std::string, std::string> uriMap = {
      {"/hn/top", kAPITopStories},
      {"/hn/show", kAPIShowStories},
      {"/hn/ask", kAPIAskStories},
      {"/hn/new", kAPINewStories},
  };

  ItemIds idsTop;
  // ItemIds idsBest;
  ItemIds idsShow;
  ItemIds idsAsk;
  ItemIds idsNew;

  void requestHelper(int id) {
    auto url = getItemURI(id);
    itemRequestCache =
        rttt::request::push(std::move(itemRequestCache), url, id);
  }

  ItemContainer items = ItemContainer(
      300, 10, 1900,
      [this](const int &key, [[maybe_unused]] const rttt::ItemVariant &value) {
        this->requestHelper(key);
      });

  uint64_t lastUpdatePoll_s = 0;

private:
};

std::set<std::string> get_known_paths(const hackernews::state &state) {
  std::set<std::string> set;
  for (auto &pair : state.uriMap)
    set.insert(pair.first);
  return set;
}

template <typename T>
auto item_view_and_mark_active(T &items, const std::vector<int> &item_ids) {
  return item_ids | std::views::filter([&items](int id) {
           if (items.contains(id) &&
               !std::holds_alternative<hackernews::Story>(items.at(id)))
             return false;
           return true;
         }) |
         std::views::transform([&items](int id) {
           if (!items.contains(id)) {
             items.insert({id, hackernews::Story()});
           } else {
             items.mark_active(id);
           }
           return items.at(id);
         });
}

/*
 * Implementing the new thing API
 */
bool is_valid_path(std::string_view name) {
  // TODO: Ideally we should not split it twice (here and in parse_path)
  auto v = rttt::text::split(name, "/");
  return (v.size() > 0 && v[0] == "hn" &&
          (v[1] == "top" || v[1] == "ask" || v[1] == "show" || v[1] == "new"));
}

bool is_valid_path(const rttt::Path &path) {
  // TODO: should we rely on path.type if it is present?
  return (path.parts.size() == 2 || path.parts.size() == 4) &&
         path.parts[0] == "hn" &&
         (path.parts[1] == "top" || path.parts[1] == "ask" ||
          path.parts[1] == "show" || path.parts[1] == "new");
}

Path parse_path(std::string_view name) {
  auto path = rttt::parse_path(name);
  return path;
}

auto setup([[maybe_unused]] state &state,
           [[maybe_unused]] nlohmann::json config) {

  state.story_ids_by_path.on_timeout =
      [&state](const std::string &key,
               [[maybe_unused]] const std::vector<int> &value) {
        auto url = state.uriMap[key];
        logger::push("HN: Updating {}", url);
        pathRequestCache =
            rttt::request::push(std::move(pathRequestCache), url, key);
      };
}

auto update(state &state) { return state.update(); }

auto retrieve_path_view(state &state, const rttt::Path &path) {
  logger::log_ifnot(is_valid_path(path));
  if (path.parts.size() == 2) {
    logger::log_ifnot(state.uriMap.contains(path.name));
    if (!state.story_ids_by_path.contains(path.name)) {
      state.story_ids_by_path.insert({path.name, {}});
    }
    state.story_ids_by_path.mark_active(path.name);
    return;
  }

  int id = std::stoi(path.id);
  if (!state.items.contains(id)) {
    state.items.insert({id, rttt::hackernews::Story()});
  } else {
    state.items.mark_active(id);
  }
  if (!state.comment_view_store.contains(id)) {
    state.comment_view_store.insert({id, {id}});
  } else {
    state.comment_view_store.mark_active(id);
  }
  return;
}

struct dig_helper {
  state *state_ptr = nullptr;
  rttt::active_storage<std::string, view::item_state> *item_view_states_ptr =
      nullptr;

  std::vector<int> *operator()(const int &id) {
    logger::log_ifnot(state_ptr != nullptr);
    logger::log_ifnot(item_view_states_ptr != nullptr);
    if (!state_ptr->items.contains(id) ||
        view::is_collapsed(*item_view_states_ptr, std::to_string(id)))
      return nullptr;
    auto &item = state_ptr->items.at(id);
    std::vector<int> *res = nullptr;
    std::visit([&res](auto &data) {
                 if constexpr (requires { res = &data.kids; }) {
                    res = &data.kids;
                 }
               }, item);
    return res;
  }
};

auto comment_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  int id = std::stoi(path.id);

  logger::log_ifnot(state.comment_view_store.contains(id));
  auto &item_ids = state.comment_view_store.at(id);

  dig_helper dig_f;
  dig_f.state_ptr = &state;
  dig_f.item_view_states_ptr = &item_view_states;
  return rttt::flat_view_with_depth(item_ids, dig_f) |
         std::views::transform([&items = state.items](auto pair) {
           if (!items.contains(pair.second)) {
             items.insert({pair.second, rttt::hackernews::Comment()});
           } else {
             items.mark_active(pair.second);
           }
           return std::pair(pair.first, items.at(pair.second));
         });
}

auto story_view(state &state, const rttt::Path &path) {
  logger::log_ifnot(state.story_ids_by_path.contains(path.name));
  return item_view_and_mark_active(state.items,
                                   state.story_ids_by_path.at(path.name)) |
         //std::views::filter(
         //    [](auto item) { return rttt::try_id_string(item).has_value(); }) |
         std::views::transform([](auto item) { return std::pair(0, item); });
}

auto try_path_view(
    state &state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states) {
  logger::log_ifnot(is_valid_path(path));
  std::optional<
      std::variant<decltype(story_view(state, path)),
                   decltype(comment_view(state, path, item_view_states))>>
      opt(std::nullopt);
  if (path.parts.size() == 2) {
    if (state.story_ids_by_path.contains(path.name)) {
      auto view = story_view(state, path);
      if (!view.empty())
        opt = view;
    }
  } else {
    if (state.comment_view_store.contains(std::stoi(path.id)))
      opt = comment_view(state, path, item_view_states);
  }
  return opt;
}

} // namespace hackernews
} // namespace rttt
