#pragma once

/*
 * This file handles the things (i.e. the different souces)
 */
#include "fmt/format.h"

#include "ftxui/component/component.hpp"

#include "rttt.hpp"

#include "rttt/command.hpp"
#include "rttt/hackernews.hpp"
#include "rttt/prompt.hpp"
#include "rttt/reddit.hpp"
#include "rttt/rss.hpp"
#include "rttt/twitter.hpp"
#include "rttt/ui_ftxui.hpp"

namespace rttt {
namespace thing {

std::optional<rttt::Path> parse_path(std::string_view name) {
  if (reddit::is_valid_path(name))
    return reddit::parse_path(name);
  else if (hackernews::is_valid_path(name))
    return hackernews::parse_path(name);
  else if (twitter::is_valid_path(name))
    return twitter::parse_path(name);
  else if (rss::is_valid_path(name))
    return (rss::parse_path(name));
  return std::nullopt;
}

std::optional<rttt::Path> focus_path(rttt::Path &&path,
                                     const ItemVariant &focus_item) {
  // TODO: Use try_with here
  auto maybe_id = try_id_string(focus_item);
  if (!maybe_id || path.mode == list_mode::feed ||
      path.mode == list_mode::comment)
    return path;
  logger::log_ifnot(path.parts.size() <= 2);
  if (rss::is_valid_path(path)) {
    auto maybe_key = std::visit(
        [](const auto &data) {
          std::optional<std::string> opt;
          if constexpr (requires { data.key; }) {
            opt = data.key;
          }
          return opt;
        },
        focus_item);
    if (!maybe_key)
      return path;
    // Need the focus item, to get its key ("/rss/{}", focus_item.key)
    return rss::parse_path(fmt::format("/rss/{}", maybe_key.value()));
  }
  auto pth = fmt::format("{}/comments/{}", path.name, maybe_id.value());
  return parse_path(pth);
}

std::optional<rttt::Path> unfocus_path(rttt::Path &&path) {
  return parse_path(path.basename);
}

std::string path_url(const rttt::Path &path) {
  if (hackernews::is_valid_path(path)) {
    return std::string("https://news.ycombinator.com/item?id=") + path.id;
  } else if (reddit::is_valid_path(path)) {
    auto uri = reddit::getURIFromPath(path);
    rttt::replaceAll(uri, "oauth", "www");
    return uri;
  } else if (twitter::is_valid_path(path)) {
    return std::string("https://nitter.net/") + path.parts[1];
  }
  return {};
}

struct state {
  twitter::state twitter_state;
  rss::state rss_state;
  hackernews::state hackernews_state;
  reddit::state reddit_state;

  command::list commands;
};

bool catch_ui_event(state &state, ui::state &ui_state, const rttt::Path &path,
                    const ItemVariant &item, ftxui::Event event) {
  if (reddit::is_valid_path(path)) {
    return reddit::catch_ui_event(state.reddit_state, ui_state, path, item,
                                  event);
  }
  return false;
}

std::string window_header(const state &state, const rttt::Path &path) {
  // TODO: Header should be responsibility of sources. Then we also don't have
  // to pass username anymore
  if (reddit::is_valid_path(path)) {
    if (!state.reddit_state.username.empty())
      return fmt::format("[R] Reddit ({} on {})", state.reddit_state.username,
                         path.name);
    return fmt::format("[R] Reddit ({})", path.name);
  } else if (rss::is_valid_path(path)) {
    return fmt::format("{} ({})", "RSS/Atom", path.name);
  } else if (twitter::is_valid_path(path)) {
    return fmt::format("{} ({})", "[TW] Twitter", path.name);
  }
  return fmt::format("{} ({})", "[Y] Hacker News", path.name);
}

auto default_view_mode(const rttt::Path &path) {
  if (path.mode == list_mode::comment)
    return view::layout::text | view::layout::spacing;
  if (path.mode == list_mode::feed)
    return view::layout::text | view::layout::numbering | view::layout::spacing;
  return view::layout::numbering;
}

auto setup(state &state, nlohmann::json &config) {
  twitter::setup(state.twitter_state, config);
  rss::setup(state.rss_state, config);
  hackernews::setup(state.hackernews_state, config);
  reddit::setup(state.reddit_state, config);
}

auto setup_commands(state &state, ui::state &ui_state) {
  state.commands =
      state.commands |
      push("log",
           command::command() |
               command::function([](const std::string_view msg) {
                 logger::push("{}", msg);
                 return true;
               }) |
               command::help(" <msg>: Log a message to the messages window")) |
      push("help",
           command::command() | command::function([&state, &ui_state]() {
             auto v = state.commands | command::as_help();
             ui_state.popup = ui::create_list_popup(ui_state, v);
             return true;
           }) | command::function([&state, &ui_state](std::string_view filter) {
             auto v =
                 state.commands | command::as_help(fmt::format("{}", filter));
             ui_state.popup = ui::create_list_popup(ui_state, v);
             return true;
           }) | command::help(" Print help for the different commands")) |
      rss::commands(state.rss_state);
}

bool update(state &state) {
  return twitter::update(state.twitter_state) || rss::update(state.rss_state) ||
         hackernews::update(state.hackernews_state) ||
         reddit::update(state.reddit_state);
}

void retrieve_path_view(state &state, const rttt::Path &path) {
  if (reddit::is_valid_path(path)) {
    reddit::retrieve_path_view(state.reddit_state, path);
  } else if (twitter::is_valid_path(path)) {
    twitter::retrieve_path_view(state.twitter_state, path);
  } else if (rss::is_valid_path(path)) {
    rss::retrieve_path_view(state.rss_state, path);
  } else if (hackernews::is_valid_path(path)) {
    hackernews::retrieve_path_view(state.hackernews_state, path);
  }
}

template <typename T>
auto dispatch_helper(
    T &value, ui::scroll_window_state &window_state,
    const rttt::active_storage<std::string, view::item_state> &item_view_states,
    int width, int height) {
  return std::visit(
      [&](auto &v) {
        return rttt::ui::draw_items(v, window_state, item_view_states, width,
                                    height);
      },
      value);
}

auto draw_path_view(
    state &state, ui::scroll_window_state &window_state, const rttt::Path &path,
    rttt::active_storage<std::string, view::item_state> &item_view_states,
    int width, int height) {
  if (reddit::is_valid_path(path)) {
    auto maybe_view =
        reddit::try_path_view(state.reddit_state, path, item_view_states);
    if (maybe_view.has_value())
      return dispatch_helper(maybe_view.value(), window_state, item_view_states,
                             width, height);
  } else if (twitter::is_valid_path(path)) {
    auto maybe_view =
        twitter::try_path_view(state.twitter_state, path, item_view_states);
    if (maybe_view.has_value())
      return dispatch_helper(maybe_view.value(), window_state, item_view_states,
                             width, height);
  } else if (hackernews::is_valid_path(path)) {
    auto maybe_view = hackernews::try_path_view(state.hackernews_state, path,
                                                item_view_states);
    if (maybe_view.has_value())
      return dispatch_helper(maybe_view.value(), window_state, item_view_states,
                             width, height);
  } else if (rss::is_valid_path(path)) {
    auto maybe_view =
        rss::try_path_view(state.rss_state, path, item_view_states);
    if (maybe_view.has_value())
      return dispatch_helper(maybe_view.value(), window_state, item_view_states,
                             width, height);
  }
  std::vector<std::pair<size_t, rttt::ItemVariant>> items;
  return rttt::ui::draw_items(items, window_state, item_view_states, width,
                              height);
}

auto completions(const thing::state &state) {
  return with([&state](prompt::state &prompt_state) -> prompt::state & {
    prompt_state.completer.matches | rss::completions(state.rss_state) |
        with([&state](auto &set) -> decltype(set) & {
          for (auto &el : state.commands | command::as_completions()) {
            // FIXME: Commands appear in the log, but seemingly not in the
            // actual matcher
            set.insert(el);
          }
          return set;
        });
    return prompt_state;
  });
}

auto execute_command([[maybe_unused]] const std::string &cmd) {
  return with(
      [cmd](auto &state) { return state.commands | command::execute(cmd); });
}
} // namespace thing
} // namespace rttt
