#pragma once

#include <chrono>
#include <functional>
#include <iterator>
#include <ranges>
#include <unordered_map>

#include <nlohmann/json.hpp>

#include "rttt/logger.hpp"

namespace rttt {
inline uint64_t current_time() {
  return std::chrono::duration_cast<std::chrono::seconds>(
             std::chrono::system_clock::now().time_since_epoch())
      .count();
}

inline bool timeout(uint64_t last, size_t seconds = 300) {
  return current_time() - last > seconds;
}

template <typename K, typename V>
// Should be private if this was a module
struct value_wrapper {
  V value;
  bool need_update = true;
  uint64_t last_update = 0;

  // Will need to be set to now on first use
  uint64_t last_active = 0;
};

template <typename K, typename V> struct active_storage {
  std::unordered_map<K, value_wrapper<K, V>> storage;

  typedef V mapped_value;

  /// Timeout period (in seconds)
  uint64_t timeout = 0;

  /// The minimal timeout to respect even if need_update is true
  uint64_t minimal_timeout = 0;

  /// End of life period. After end the stored value will be removed (0 for
  /// never).
  uint64_t end_of_life = 0;

  std::function<void(const K &key, V &value)> on_timeout;

  active_storage() = default;
  active_storage(const active_storage<K, V> &) = default;
  active_storage(active_storage<K, V> &&) = default;

  active_storage(uint64_t timeout, uint64_t minimal_timeout,
                 uint64_t end_of_life,
                 std::function<void(const K &key, V &value)> on_timeout)
      : timeout(timeout), minimal_timeout(minimal_timeout),
        end_of_life(end_of_life), on_timeout(on_timeout) {}

  active_storage(uint64_t timeout, uint64_t minimal_timeout,
                 uint64_t end_of_life)
      : timeout(timeout), minimal_timeout(minimal_timeout),
        end_of_life(end_of_life) {}

  active_storage<K, V> &operator=(const active_storage<K, V> &) = default;
  active_storage<K, V> &operator=(active_storage<K, V> &&) = default;

  void insert(const std::pair<K, V> &pair) {
    value_wrapper<K, V> wrapper;
    wrapper.last_active = current_time();
    wrapper.value = pair.second;
    storage.insert({pair.first, wrapper});
  }

  bool contains(const K &key) const { return storage.contains(key); }

  auto find(const K &key) const { return storage.find(key); }
  auto begin() const { return storage.begin(); }
  auto end() const { return storage.end(); }

  void erase(const K &key) {
    logger::log_ifnot(storage.contains(key));
    storage.erase(key);
  }

  V &at(const K &key) {
    logger::log_ifnot(storage.contains(key));
    return storage.at(key).value;
  }

  const V &at(const K &key) const {
    logger::log_ifnot(storage.contains(key));
    return storage.at(key).value;
  }

  void mark_active(const K &key) {
    logger::log_ifnot(storage.contains(key));
    storage.at(key).last_active = current_time();
  }

  void mark_for_update(const K &key) {
    logger::log_ifnot(storage.contains(key));
    storage.at(key).need_update = true;
  }

  void mark_updated(const K &key) {
    logger::log_ifnot(storage.contains(key));
    storage.at(key).need_update = false;
    storage.at(key).last_update = rttt::current_time();
  }

  bool update() {
    bool updated = false;
    if (end_of_life > 0) {
      auto it = storage.begin();
      while (it != storage.end()) {
        if (rttt::timeout(it->second.last_active, end_of_life)) {
          updated = true;
          it = storage.erase(it);
        } else {
          ++it;
        }
      }
    }
    if (on_timeout) {
      for (auto &pair : storage) {
        if ((pair.second.need_update &&
             rttt::timeout(pair.second.last_update, minimal_timeout)) ||
            (timeout > 0 && rttt::timeout(pair.second.last_update, timeout))) {
          on_timeout(pair.first, pair.second.value);
          pair.second.need_update = false;
          pair.second.last_update = current_time();
          updated = true;
        }
      }
    }
    return updated;
  }

  size_t size() const { return storage.size(); }
  bool empty() const { return storage.empty(); }
};

template <typename K, typename V>
void to_json(nlohmann::json &j, const value_wrapper<K, V> &v) {
  j = nlohmann::json{{"value", v.value},
                     {"need_update", v.need_update},
                     {"last_update", v.last_update},
                     {"last_active", v.last_active}};
}

template <typename K, typename V>
void from_json(const nlohmann::json &j, value_wrapper<K, V> &v) {
  j.at("value").get_to(v.value);
  j.at("need_update").get_to(v.need_update);
  j.at("last_update").get_to(v.last_update);
  j.at("last_active").get_to(v.last_active);
}

template <typename K, typename V>
void to_json(nlohmann::json &j, const active_storage<K, V> &s) {
  j = nlohmann::json{{"minimal_timeout", s.minimal_timeout},
                     {"timeout", s.timeout},
                     {"end_of_life", s.end_of_life},
                     {"storage", s.storage}};
}

template <typename K, typename V>
void from_json(const nlohmann::json &j, active_storage<K, V> &s) {
  j.at("minimal_timeout").get_to(s.minimal_timeout);
  j.at("timeout").get_to(s.timeout);
  j.at("end_of_life").get_to(s.end_of_life);
  j.at("storage").get_to(s.storage);
}

struct flat_view_sentinel {};

template <typename T, typename F> struct flat_view_iterator {
  std::vector<T *> containers;
  std::vector<typename T::iterator> its;

  using value_type = std::pair<size_t, typename T::value_type>;
  // No one knows what this does. All tutorials say not to worry about it and refuse to explain it, but it is needed apparently.
  using difference_type = std::ptrdiff_t;
  using iterator_tag = std::bidirectional_iterator_tag;

  F dig;

  flat_view_iterator() {}
  flat_view_iterator(const flat_view_iterator &) = default;
  flat_view_iterator(T &states, F &dig) : dig(dig) { descent(true, &states); }

  flat_view_iterator &operator=(const flat_view_iterator &) = default;

  auto operator*() const { return std::pair(its.size() - 1, (*its.back())); }

  auto &operator++() {
    auto kids = dig(*its.back());
    if (kids == nullptr || kids->empty()) {
      ++its.back();
    } else {
      descent(true, kids);
    }
    while (its.back() == containers.back()->end() && its.size() > 1) {
      ascent();
      ++its.back();
    }
    return *this;
  }

  auto operator++(int) {
    auto prev = *this;
    ++(*this);
    return prev;
  }

  auto &operator--() {
    {
      // Go up
      if (containers.size() > 1 && its.back() == containers.back()->begin()) {
        ascent();
        return *this;
      }

      --its.back();
    }

    // Go deep
    while (true) {
      auto kids = dig(*its.back());
      if (kids == nullptr || kids->empty()) {
        break;
      } else {
        descent(false, kids);
      }
    }
    return *this;
  }

  auto operator--(int) {
    auto prev = *this;
    --(*this);
    return prev;
  }

  bool operator==(const flat_view_iterator &other) const {
    return this->its.back() == other.its.back();
  }
  bool operator==(const flat_view_sentinel &) const {
    return (its.size() == 0 ||
            (its.size() == 1 && its.back() == containers.back()->end()));
  }

private:
  void ascent() {
    logger::log_ifnot(its.size() > 1);
    its.pop_back();
    containers.pop_back();
  }

  void descent(bool at_begin, T *kids) {
    containers.push_back(kids);
    if (at_begin)
      its.push_back(containers.back()->begin());
    else
      its.push_back(--(containers.back()->end()));
  }
};

template <typename T, typename F> auto flat_view_with_depth(T &states, F dig) {
  auto it = flat_view_iterator(states, dig);

  return std::ranges::subrange<flat_view_iterator<T, F>, flat_view_sentinel>(
      std::move(it), {});
}
} // namespace rttt
