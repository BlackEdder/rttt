#pragma once

#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace rttt {

namespace hackernews {
using ItemId = int;
using ItemIds = std::vector<ItemId>;

struct Story {
  std::string by = "";
  int descendants = 0;
  ItemId id = 0;
  ItemIds kids;
  int score = 0;
  uint64_t time = 0;
  std::string text = "";
  std::string title = "";
  std::string url = "";
  std::string domain = "";
};

struct Comment {
  std::string by = "";
  ItemId id = 0;
  ItemIds kids;
  ItemId parent = 0;
  std::string text = "";
  uint64_t time = 0;
};
} // namespace hackernews

namespace rss {
struct Story {
  std::string by = "";
  uint64_t time = 0;
  std::string text = "";
  std::string title = "";
  std::string domain = "";
  std::string url = "";

  int id = 0;
  std::string key = "";
};
} // namespace rss

// Forward declarations
namespace reddit {
struct Story;
struct Comment;
} // namespace reddit
//
namespace twitter {
struct tweet {
  std::string id;
  std::string text;
  std::vector<std::string> kids;

  int score = 0;
  int descendants = 0;

  std::string title = "";
  int time = 0;
  std::string by = "";
};
} // namespace twitter

struct unknown_type {};

using ItemVariant =
    std::variant<reddit::Comment, reddit::Story, hackernews::Comment,
                 hackernews::Story, rss::Story, rttt::unknown_type,
                 twitter::tweet>;

namespace reddit {
struct Story {
  std::string by = "";
  int descendants = 0;
  int score = 0;
  int vote = 0;
  uint64_t time = 0;
  std::string text = "";
  std::string title = "";
  std::string domain = "";
  std::string url = "";
  std::string fullname = "";

  std::vector<ItemVariant> kids;

  std::string id = "";
};

struct Comment {
  std::string by = "";
  std::string text = "";
  std::string id = "";
  std::string fullname = "";
  uint64_t time = 0;
  int score = 0;
  int vote = 0;

  std::vector<ItemVariant> kids;
};
} // namespace reddit

template <typename T> std::string safe_to_string(const T &item) {
  if constexpr (requires(const T &t) { std::to_string(t); }) {
    return std::to_string(item);
  } else {
    return item;
  }
}

/*template <typename T>
inline std::optional<std::string> try_id_string(const T &item) {
  if constexpr (requires { item.id; }) {
    return safe_to_string(item.id);
  } else {*/
inline std::optional<std::string> try_id_string(const ItemVariant &item) {
  std::string id;
  std::visit(
      [&id](const auto &item) {
        if constexpr (requires { item.id; }) {
          id = safe_to_string(item.id);
        }
      },
      item);
  if (!id.empty() && id != "0")
    return id;
  return std::nullopt;
}

inline std::optional<uint64_t> try_time(const ItemVariant &item) {
  std::optional<uint64_t> opt;
  std::visit(
      [&opt](const auto &data) {
        if constexpr (requires { data.time; }) {
          opt = data.time;
        }
      },
      item);
  return opt;
}

std::optional<int> try_vote(const ItemVariant &item) {
  std::optional<int> opt;
  std::visit(
      [&opt](const auto &data) {
        if constexpr (requires { data.vote; }) {
          opt = data.vote;
        }
      },
      item);
  return opt;
}

std::optional<std::string> try_fullname(const ItemVariant &item) {
  std::optional<std::string> opt;
  std::visit(
      [&opt](const auto &data) {
        if constexpr (requires { data.fullname; }) {
          opt = data.fullname;
        }
      },
      item);
  return opt;
}

inline std::optional<std::string> try_url(const ItemVariant &item) {
  std::optional<std::string> opt;
  std::visit(
      [&opt](const auto &data) {
        if constexpr (requires { data.url; })
          opt = data.url;
      },
      item);
  return opt;
}

inline std::optional<std::string> try_title(const ItemVariant &item) {
  std::optional<std::string> opt;
  std::visit(
      [&opt](const auto &data) {
        if constexpr (requires { data.title; })
          opt = data.title;
      },
      item);
  return opt;
}

inline std::optional<std::string> try_text(const ItemVariant &item) {
  std::optional<std::string> opt;
  std::visit(
      [&opt](const auto &data) {
        if constexpr (requires { data.text; }) {
          opt = data.text;
        }
      },
      item);
  return opt;
}
} // namespace rttt
