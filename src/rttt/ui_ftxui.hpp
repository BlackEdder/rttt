#pragma once

#include <cstddef>
#include <string>

#include "fmt/format.h"

#include "ftxui/component/component.hpp"
#include "ftxui/dom/elements.hpp"

#include "rttt/item.hpp"
#include "rttt/logger.hpp"
#include "rttt/text.hpp"
#include "rttt/view.hpp"

namespace rttt {
namespace ui {
struct popup_state {
  std::string title = "";
  ftxui::Element content;

  std::function<bool(ftxui::Event)> event_handler;
};

struct state {
  bool prompt_open = false;
  bool status_open = false;

  std::optional<popup_state> popup;

  // This includes the status "header"
  size_t status_window_height = 5;

  rttt::active_storage<std::string, view::item_state> item_view_states;
};

auto create_url_popup(state &ui_state, std::vector<std::string> urls) {
  popup_state state;

  std::string chars = "0123456789abcdefghijklmnopqrstuvwxyz";
  ftxui::Elements elements = {ftxui::separatorEmpty()};
  for (size_t i = 0; i < std::min(urls.size(), chars.size()); ++i) {
    elements.push_back(ftxui::text(fmt::format(" [{}] {} ", i, urls[i])));
  }
  elements.push_back(ftxui::separatorEmpty());

  state.title = " Press character to open url (escape to cancel) ";

  state.content = ftxui::vbox(elements);

  state.event_handler = [&ui_state, urls, chars](ftxui::Event event) {
    if (event == ftxui::Event::Escape) {
      ui_state.popup = std::nullopt;
      return true;
    }
    for (size_t i = 0; i < std::min(chars.size(), urls.size()); ++i) {
      if (event == ftxui::Event::Character(chars[i])) {
        rttt::openInBrowser(urls[i].c_str());
        ui_state.popup = std::nullopt;
        return true;
      }
    }
    return false;
  };

  return state;
}

auto create_list_popup(state &ui_state, std::vector<std::string> lines) {
  popup_state state;

  ftxui::Elements elements = {ftxui::separatorEmpty()};
  for (auto &line : lines) {
    elements.push_back(ftxui::text(fmt::format(" {} ", line)));
  }
  elements.push_back(ftxui::separatorEmpty());

  state.title = " Press character to open url (escape to cancel) ";

  state.content = ftxui::vbox(elements);

  state.event_handler = [&ui_state](ftxui::Event event) {
    if (event == ftxui::Event::Escape) {
      ui_state.popup = std::nullopt;
      return true;
    }
    return false;
  };

  return state;
}



auto create_help_popup(state &ui_state) {
  popup_state state;

  state.title = " Help (press escape to close) ";

  state.content = ftxui::vbox(
      {ftxui::separatorEmpty(),
       ftxui::text("    ?           - toggle Help window    "),
       ftxui::text("    /           - Open a new path by typing it (e.g. "
                   "/r/front, /hn/top)   "),
       ftxui::text("    s           - toggle Messages window    "),
       ftxui::text("    g           - go to top    "),
       ftxui::text("    o           - open in browser    "),
       ftxui::text("    j/k         - navigate items    "),
       ftxui::text(
           "    J/K         - mark items read/unread while navigating    "),
       ftxui::text("    Space       - collapse comments    "),
       ftxui::text("    l           - open comments/feed   "),
       ftxui::text("    h           - close comments    "),
       ftxui::text("    v           - toggle view mode    "),
       ftxui::text(
           "    a/z         - up/down vote a comment (only when logged in)  "),
       ftxui::text("    u           - login to reddit  "),
       ftxui::text("    q           - quit    "), ftxui::separatorEmpty()});

  state.event_handler = [&](ftxui::Event event) {
    if (event == ftxui::Event::Escape) {
      ui_state.popup = std::nullopt;
      return true;
    }
    return false;
  };

  return state;
}

struct scroll_window_state {
  view::layout layout = view::layout::base | view::layout::numbering;

  ItemVariant highlighted_item = rttt::unknown_type();

  bool goto_next_item = false;
  bool goto_prev_item = false;

  bool goto_top = false;
  bool goto_bottom = false;
};

size_t scroll_window_height(const state &state, const size_t screen_height) {
  size_t working_height = screen_height - 1;
  if (state.prompt_open)
    --working_height;
  if (state.status_open)
    working_height -= state.status_window_height;
  return working_height;
}

auto paragraph_and_height(const std::string_view &text, int width) {
  auto strings = rttt::text::wrap(text, width);

  ftxui::Elements lines;
  lines.reserve(strings.size());
  for (auto line : strings)
    lines.push_back(ftxui::text(line));

  return std::make_tuple(vbox(lines), lines.size());
}

template <typename T> std::string build_byline(const T &item, bool collapsed) {
  std::string score_string = "";
  if constexpr (requires() { item.vote; }) {
    score_string = fmt::format("{}({}) points ", item.score, item.vote);
  } else if constexpr (requires { item.score; }) {
    score_string = fmt::format("{} points ", item.score);
  }

  std::string author_string = "";
  if constexpr (requires { item.by; }) {
    if (!item.by.empty())
      author_string = fmt::format("by {} ", item.by);
  }

  std::string comments_string = "";
  if constexpr (requires { item.descendants; }) {
    comments_string = fmt::format("{} comments ", item.descendants);
  }

  std::string collapsed_string = "[-]";
  if (collapsed)
    collapsed_string = "[+]";

  return fmt::format("{}{}{} ago | {}{}", score_string, author_string,
                     rttt::text::time_since_string(item.time), comments_string,
                     collapsed_string);
}

template <typename T>
auto draw_item(const T &item, size_t counter, size_t width, size_t indent,
               bool highlighted, bool read, bool collapsed,
               view::layout layout) {
  // width - 6 - 2 * indent
  ftxui::Elements row;
  size_t height = 0;

  // All items except unknown item have a time
  if constexpr (requires { item.time; }) {
    auto base_indent = 2;
    if (layout == view::layout::numbering)
      base_indent += 4;
    int item_width = width - base_indent - 2 * indent;

    if (highlighted)
      row.push_back(ftxui::separatorHeavy());
    else
      row.push_back(ftxui::separatorEmpty());

    // Optional title
    int title_height = 0;
    auto title_box = ftxui::vbox();
    if constexpr (requires { item.title; }) {
      std::string domain_string = "";
      if constexpr (requires { item.domain; }) {
        if (!item.domain.empty())
          domain_string = fmt::format(" ({}) ", item.domain);
      }

      std::tie(title_box, title_height) =
          paragraph_and_height(item.title, item_width - domain_string.size());
      title_box = ftxui::hbox({title_box | ftxui::bold | ftxui::xflex_grow,
                               ftxui::text(domain_string)});
    }

    for (size_t j = 0; j < indent; ++j) {
      row.push_back(ftxui::text("  "));
    }

    if (layout == view::layout::numbering) {
      std::string id_str = fmt::format("{:4} ", counter);
      row.push_back(ftxui::text(id_str));
    }

    // Optional text
    int text_height = 0;
    auto text_box = ftxui::vbox();
    if constexpr (requires { item.text; }) {
      if (layout == view::layout::text && !collapsed) {
        std::tie(text_box, text_height) =
            paragraph_and_height(item.text, item_width);
      }
    }

    std::string by_line = build_byline(item, collapsed);

    if (!read)
      row.push_back(ftxui::vbox(
          {title_box, text_box, ftxui::text(by_line) | ftxui::dim}));
    else
      row.push_back(ftxui::vbox({title_box | ftxui::dim, text_box | ftxui::dim,
                                 ftxui::text(by_line) | ftxui::dim}));

    if (highlighted)
      row.front() |= ftxui::focus;
    if (layout == view::layout::spacing)
      return std::make_tuple(
          ftxui::vbox({ftxui::hbox(row), ftxui::separatorEmpty()}),
          height + title_height + text_height + 2);
    return std::make_tuple(ftxui::hbox(row),
                           height + title_height + text_height + 1);
  } else {
    return std::make_tuple(ftxui::hbox(row), height);
  }
}

bool same_item(const ItemVariant &var1, const ItemVariant &var2) {
  auto maybe_id1 = try_id_string(var1);
  if (!maybe_id1.has_value())
    return false;

  auto maybe_id2 = try_id_string(var2);
  if (!maybe_id2.has_value())
    return false;

  return maybe_id1.value() == maybe_id2.value();
}

// Draw a scrollable window of items
template <typename T>
auto draw_items(
    T &items, scroll_window_state &state,
    const rttt::active_storage<std::string, view::item_state> &item_view_states,
    int width, int height) {
  using namespace ftxui;
  Elements elements;

  if (state.goto_next_item && state.goto_prev_item) {
    // Can happen when someone presses j and k in quick succession
    state.goto_next_item = false;
    state.goto_prev_item = false;
  }

  if (width == 0)
    return vbox(elements) | flex | yframe | focusPosition(0, 0);

  elements.push_back(separatorEmpty());

  size_t counter = 1;
  int height_after_focus = -1;

  // Keep track of skipped items (when still loading)
  int no_skipped_items = 0;

  auto it = items.begin();
  while (it != items.end() && no_skipped_items < 10) {
    size_t indent = (*it).first;
    rttt::ItemVariant item = (*it).second;
    auto maybe_id = try_id_string(item);
    if (!maybe_id.has_value()) {
      ++it;
      ++no_skipped_items;
      continue;
    }

    if (!try_id_string(state.highlighted_item).has_value() || state.goto_top) {
      state.highlighted_item = (*it).second;
      if (state.goto_top) {
        state.goto_top = false;
      }
    }

    // Figure out highlight status
    auto it_next = it++;
    std::swap(it, it_next);
    bool highlighted = false;
    if (state.goto_next_item &&
        same_item((*it).second, state.highlighted_item) &&
        it_next != items.end()) {
      state.highlighted_item = rttt::unknown_type();
      state.goto_next_item = false;
    } else if (state.goto_prev_item && it == items.begin() &&
               same_item((*it).second, state.highlighted_item)) {
      // We are at the top/first item
      state.goto_prev_item = false;
      height_after_focus = 0;
      highlighted = true;
    } else if (state.goto_prev_item && it_next != items.end()) {
      if (same_item((*it_next).second, state.highlighted_item)) {
        highlighted = true;
        state.goto_prev_item = false;
        state.highlighted_item = (*it).second;
        height_after_focus = 0;
      }
    } else if (same_item((*it).second, state.highlighted_item)) {
      highlighted = true;
      height_after_focus = 0;
    };

    const auto &[item_element, item_height] = std::visit(
        [&](const auto &item_visit) {
          return draw_item(
              item_visit, counter, width, indent, highlighted,
              view::is_read(item_view_states, maybe_id.value()),
              view::is_collapsed(item_view_states, maybe_id.value()),
              state.layout);
        },
        item);

    if (height_after_focus != -1)
      height_after_focus += item_height;

    elements.push_back(hbox(item_element | size(HEIGHT, EQUAL, item_height)));

    if (height_after_focus > height)
      break;

    ++counter;
    ++it;
  }

  // Reached the end without finding the highlighted item so reset it
  if (height_after_focus == -1)
    state.highlighted_item = rttt::unknown_type();

  state.goto_next_item = false;
  state.goto_prev_item = false;
  state.goto_top = false;
  // FIXME: goto bottom currently not supported. Should we remove it?
  state.goto_bottom = false;

  return vbox(elements) | flex | yframe;
}

} // namespace ui
} // namespace rttt
