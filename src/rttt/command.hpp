#pragma once

#include <string>
#include <functional>
#include <optional>
#include <map>
#include <memory>
#include <ranges>

#include "fmt/format.h"

#include "rttt/functional.hpp"
#include "rttt/text.hpp"

namespace rttt {
namespace command {
struct command_state {
  std::function<bool()> func;
  std::function<bool(const std::string_view url)> func_with_arg;

  std::map<std::string, std::shared_ptr<command_state>> commands;

  std::optional<std::string> help;
};

using command = std::shared_ptr<command_state>;
using list = std::map<std::string, command>;

auto help(const std::string &message) {
  return with([message](command &cmd) -> command & {
    if (!cmd) {
      cmd = std::make_shared<command_state>();
    }
    cmd->help = std::move(message);
    return cmd;
  });
}

auto function(const std::function<bool(const std::string_view argument)> &f) {
  return with([f](command &cmd) -> command & {
    if (!cmd) {
      cmd = std::make_shared<command_state>();
    }
    cmd->func_with_arg = f;
    return cmd;
  });
}

auto function(const std::function<bool()> &f) {
  return with([f](command &cmd) -> command & {
    if (!cmd) {
      cmd = std::make_shared<command_state>();
    }
    cmd->func = f;
    return cmd;
  });
}

void push_helper(list &commands, const std::string &name, command cmd) {
  auto names = rttt::text::split(name, " ", 2);
  if (names.size() == 1) {
    commands.insert({names[0], cmd});
  } else {
    if (!commands.contains(names[0])) {
      auto new_cmd = std::make_shared<command_state>();
      commands.insert({names[0], new_cmd});
    }
    push_helper(commands.at(names[0])->commands, names[1], cmd);
  }
}

auto push(const std::string &name, command cmd) {
  return with([&name, cmd](list &commands) -> list & {
    push_helper(commands, name, cmd);
    return commands;
  });
}

auto push(const std::string &name,
          std::function<bool(const std::string_view argument)> f) {
  return with([f, &name](list &commands) -> list & {
    auto command = std::make_shared<command_state>() | function(f);
    commands | push(name, command);
    return commands;
  });
}

auto push(const std::string &name, std::function<bool()> f) {
  return with([f, &name](list &commands) -> list & {
    auto command = std::make_shared<command_state>() | function(f);
    commands | push(name, command);
    return commands;
  });
}

auto push(const std::string &name, list &subcommands) {
  return with([&name, &subcommands](list &commands) -> list & {
    auto command = std::make_shared<command_state>();
    command->commands = subcommands;
    commands | push(name, command);
    return commands;
  });
}

auto push(const std::string &name, list &&subcommands) {
  return with([&name, &subcommands](list &commands) -> list & {
    auto command = std::make_shared<command_state>();
    command->commands = std::move(subcommands);
    commands | push(name, command);
    return commands;
  });
}

with<std::function<bool(list &)>>
execute(const std::string &name,
        const std::optional<std::string_view> arg = std::nullopt) {
  std::function<bool(list &)> f = [&name, arg](list &commands) -> bool {
    if (name.empty())
      return false;

    auto nm = name;
    if (nm[0] == ':')
      nm = nm.substr(1);

    if (!commands.contains(nm)) {
      if (!arg) {
        auto v = rttt::text::split(nm, " ", 2);
        if (v.size() > 1) {
          return commands | execute(v[0], v[1]);
        }
      }
      return false;
    }

    auto &cmd = commands[nm];

    if (!arg) {
      if (cmd->func)
        return cmd->func();
      return false;
    }

    auto v = rttt::text::split(arg.value(), " ", 2);
    if (v.size() == 1 && cmd->commands | execute(v[0]))
      return true;
    else if (v.size() == 2 && cmd->commands | execute(v[0], v[1]))
      return true;

    if (cmd->func_with_arg)
      return cmd->func_with_arg(arg.value());

    return false;
  };
  return with(f);
}

with<std::function<std::vector<std::string>(const list &)>>
as_completions(bool with_colon = true) {
  std::function<std::vector<std::string>(const list &)> f =
      [with_colon](const list &commands) {
        std::vector<std::string> v;
        for (auto &pair : commands) {
          std::string base = pair.first;
          if (with_colon)
            base = fmt::format(":{}", pair.first);
          v.push_back(base);
          auto subv = pair.second->commands | as_completions(false);
          for (auto &s : subv)
            v.push_back(fmt::format("{} {}", base, s));
        }
        return v;
      };
  return with(f);
}

with<std::function<std::vector<std::string>(const list &)>>
as_help(const std::optional<std::string> &filter = std::nullopt,
        bool with_colon = true) {
  std::function<std::vector<std::string>(const list &)> f =
      [&filter, with_colon](const list &commands) {
        std::vector<std::string> v;
        for (auto &pair : commands) {
          std::string base = pair.first;
          if (with_colon)
            base = fmt::format(":{}", pair.first);
          if (pair.second->help.has_value())
            v.push_back(fmt::format("{}{}", base, pair.second->help.value()));

          auto subv = pair.second->commands | as_help(std::nullopt, false);
          for (auto &s : subv)
            v.push_back(fmt::format("{} {}", base, s));
        }

        // TODO: apply this filter while traversing the tree (for performance)
        if (filter) {
          auto rng = v |
                     std::views::filter(
                         [flt = filter.value()](const std::string &name) {
                           return (name.size() > flt.size() + 1 &&
                                   name.substr(1, flt.size()) == flt);
                         }) |
                     collect<std::vector<std::string>>();
          return rng;
        }
        return v;
      };
  return with(f);
}
} // namespace command

} // namespace rttt
