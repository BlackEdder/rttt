#pragma once

#include <cstddef>
#include <arpa/inet.h>
#include <string>
#include <unistd.h>

namespace rttt {
namespace socket {
struct socket {
  int server_fd;
  struct sockaddr_in address;
};

inline socket listen(size_t port) {
  // https://www.geeksforgeeks.org/socket-programming-cc/
  socket s;
  s.server_fd = ::socket(AF_INET, SOCK_STREAM, 0); // creating socket

  s.address.sin_family = AF_INET;
  s.address.sin_addr.s_addr = INADDR_ANY;
  s.address.sin_port = htons(port); // this is the port number of running server

  [[maybe_unused]] auto result = bind(s.server_fd, (struct sockaddr *)&s.address, sizeof(s.address));
  logger::log_ifnot(result == 0);

  // Only allow one connection in queue
  result = ::listen(s.server_fd, 1);
  logger::log_ifnot(result == 0);
  return s;
}

inline std::string read_reply_and_close(const socket &s) {
  char buffer[1024] = {0};
  auto connection = accept(s.server_fd, (struct sockaddr *)NULL, NULL);
  [[maybe_unused]] auto result = read(connection, buffer, 1024);
  logger::log_ifnot(result > 0);
  std::string reply = "Thank you! rttt now has the required permissions. You "
                      "can now close this page";
  result = write(connection, reply.c_str(), reply.size());
  logger::log_ifnot(result > 0);
  close(connection);
  return std::string(buffer);
}
} // namespace socket
} // namespace rttt
