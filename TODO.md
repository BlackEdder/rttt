This is a very rough list of ideas and future todo items.

# Documentation urls

[imgui](https://pixtur.github.io/mkdocs-for-imgui/site/api-imgui/ImGui--Dear-ImGui-end-user/#settingsini-utilities)

# TODOs

- [ ] Twitter support
- [ ] Add gcc version information to README

## Refactor

- [ ] Use cpr getcallback support instead of my own version of it: https://docs.libcpr.org/advanced-usage.html#asynchronous-callbacks
- [ ] Switch to a pop like interface for request (instead of try_pop_and_retrieve). This would stop us from introducing infinite loops again. Could we put this into an iterator (++ would pop last one and present next one, something like consume_iterator)? Problem with the pop on ++ might be that it does not properly work with take/the first call?
```cpp
while (!myqueue.empty())
  {
    auto el = myqueue.front();
    myqueue.pop();
  }
```
- [ ] Make reddit responsible for itself (state() to setup)
  - When using config just read it and write it when changed
- [ ] Use future_wait_list in request.hpp (or even in place off?)
- [ ] Make Path useable as map key (comparison based on name)
    - Use it for the url keys in hackernews and reddit. 
    - Easiest way is by implementing comparison operator: `auto operator<=>(Class1 const &other) const { return this->name <=> other.name;};`
    - Might want to hash it?
    - https://oleksandrkvl.github.io/2021/04/02/cpp-20-overview.html#three-way-comparison (should we imply strict ordering)?
    - Seems like we don't need an equivalence operator (`==`) https://stackoverflow.com/questions/20168173/when-using-stdmap-should-i-overload-operator-for-the-key-type
- [ ] Proper iterator for active_storage, which only exposes the key, value not the value_wrapper
- [ ] Consistent naming style. Let's try the standard library style -> underscore and lowercase everywhere, use namespaces to differentiate: request::state state; etc.
- [ ] Use raw literals wherever possible? https://www.geeksforgeeks.org/raw-string-literal-c/
- [ ] Should we keep track of trying to fetch and actual successful fetch? Or more aggressively retry on errors (make sure not to retry on errors that mean an ip/site does not exist any more).
- [ ] Reduce trying for sites/urls that consistently return an error (especially for rss?)?
 
## Features

- [ ] Save completion history
- [ ] Call from outside to open a certain path
- [ ] Save colourscheme to config
- [ ] Bold? text/font?
    - Can we rely on terminals colour/bold/etc coding?
- [ ] Improve error handling Reddit
    - Non exiting paths
    - (make it async first) Refresh after requesting new access token
- [ ] Reddit: it might be possible to get default subreddits from /subreddits when not logged in. We could use that to get tab completion working for those. Note that ideally we would then remove them again on login
- [ ] Explore performance
    - [ ] Track how many items are "visible" (e.g. above the top of the window + actually visible items). And use that to get a reasonable number of items. This might be complicated for collapsed, but since we collapse them/never load them it might also just be fine. Something to keep in mind though! ;)
      - Stop drawing couple items after they are not visible any more! (Could be fun to log how much overdrawing we are doing at the moment)
    - [ ] Consider not calling active_storage.update() on every frame
    - [ ] Ticker in active_storage, or just perform certain things less often (in main)
    - [ ] Staggered requests? We could keep track of how many futures are currently in the request queue and only add more if they go below certain point
- [ ] Reddit: load additional items when scrolling to bottom
    - [ ] Clean way to load additional stories.. There was something in the API for this
    - https://towardsdatascience.com/how-to-use-the-reddit-api-in-python-5e05ddfd1e5c has some information on getting more than a 100 using before and after
    - [ ] Load less at the start? 
- [ ] Explore other fonts/utf8. Would allow us to prettify things (nicer highlight marker as well) (first one has useful code snippets; https://pixtur.github.io/mkdocs-for-imgui/site/FONTS/) (https://pixtur.github.io/mkdocs-for-imgui/site/FAQ/#q-how-can-i-easily-use-icons-in-my-application) (https://pixtur.github.io/mkdocs-for-imgui/site/FAQ/#q-how-can-i-display-and-input-non-latin-characters-such-as-chinese-japanese-korean-cyrillic) 
- [ ] Reddit Posting of comments
- [ ] Better parseHTML
- [ ] Support reddit subreddits in the 'o' menu. I.e. if someone mentions r/neovim, then it should be possible to open /r/neovim in rttt
- [ ] Twitter access?

## Bug

- [x] Rss keeps hanging.. Let's start putting in timeouts and check for cpr::Error in Response (https://github.com/libcpr/cpr/blob/master/include/cpr/error.h)
- [ ] If we are in non text mode and we press v, while on a large_item we end up with the highlighted item off screen. Presumably because scrolltop is wrong
- [ ] Crash when pressing 'g' on an empty/loading page

