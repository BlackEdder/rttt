# Read the things tui

Read-the-things-tui (rttt) lets you read the things from the terminal. The things currently include RSS/Atom, hackernews and Reddit. It is a terminal application written in C++ and originally based on [hnterm](https://github.com/ggerganov/hnterm.git), but with added support for other things (Reddit/RSS/Atom). 

![screenshot1](https://gitlab.com/BlackEdder/rttt/-/wikis/uploads/974e093e08d802d484f0fb454fc5d933/rttt_hntop.png)

![screenshot2](https://gitlab.com/BlackEdder/rttt/-/wikis/uploads/db45a0db2ba991283fb96da6e21fd574/rttt_help.png)

## Installing

### Arch

'rttt' is included in the 'aur' repository, so you can install it from there:

```bash
yay -S rttt-git
```

### From source

'rttt' depends on 'zeromq' so make sure that is installed first (e.g. `sudo apt install libzmq3-dev` on ubuntu). After that run:

```bash
git clone https://gitlab.com/BlackEdder/rttt.git 
cd rttt 
cmake . 
make install
```

## Usage

Press (?) to open a popup with all the available key bindings. Scrolling is done with 'j' and 'k', opening comments to a story by pressing 'l' and 'h' will bring you back to the main listing. 'o' allows you to open the url in a browser. 

To switch to different readable things press '/' and type the various shortcuts to different items. For example, '/hn/top', '/hn/ask', '/hn/show' and '/hn/new' the for different hackernews pages. '/r/front' for the reddit front page and '/r/\<subreddit\>' for different subreddits. To see your rss/atom feed use '/rss'. Note that for rss support you need save an 'opml' file with your feeds as '.config/rttt/subscriptions.opml'.

By default **rttt** uses **xdg-open** to open urls. This can be changed in the configuration [open_command](https://gitlab.com/BlackEdder/rttt/-/wikis/Custom-open_command). I myself use [sesame](https://github.com/green7ea/sesame), which makes it easy to open images directly in an image viewer, movies in mpv, etc. My configuration for sesame can be found on the [wiki](https://gitlab.com/BlackEdder/rttt/-/wikis/Custom-open_command)

### Commands

New in 'rttt' is the support for basic commands. For example `:open path /rss` will open your rss subscriptions. To see a list of all the currently available commands type `:help`. Current support is limited, but we hope to start supporting more commands soon.

We have also added the 'rttt-send' binary which enables you to send commands to your open 'rttt' instance from other processes, e.g.: `rttt-send ":rss feed add <feed>"`.

## Configuration

Configuration for **rttt** is stored in '$XDG_CONFIG_HOME/rttt/config.json'.


